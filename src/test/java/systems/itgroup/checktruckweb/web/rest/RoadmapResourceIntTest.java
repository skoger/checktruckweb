package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.domain.Routers;
import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.repository.RoadmapRepository;
import systems.itgroup.checktruckweb.service.RoadmapService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.RoadmapCriteria;
import systems.itgroup.checktruckweb.service.RoadmapQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.sameInstant;
import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RoadmapResource REST controller.
 *
 * @see RoadmapResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class RoadmapResourceIntTest {

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    private static final Boolean DEFAULT_SYNCH = false;
    private static final Boolean UPDATED_SYNCH = true;

    private static final Boolean DEFAULT_VERIFIED = false;
    private static final Boolean UPDATED_VERIFIED = true;

    private static final String DEFAULT_ID_USER = "AAAAAAAAAA";
    private static final String UPDATED_ID_USER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VERIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VERIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_USER_KEY = "AAAAAAAAAA";
    private static final String UPDATED_USER_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ANNULLED = false;
    private static final Boolean UPDATED_ANNULLED = true;

    @Autowired
    private RoadmapRepository roadmapRepository;

    

    @Autowired
    private RoadmapService roadmapService;

    @Autowired
    private RoadmapQueryService roadmapQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRoadmapMockMvc;

    private Roadmap roadmap;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoadmapResource roadmapResource = new RoadmapResource(roadmapService, roadmapQueryService);
        this.restRoadmapMockMvc = MockMvcBuilders.standaloneSetup(roadmapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Roadmap createEntity(EntityManager em) {
        Roadmap roadmap = new Roadmap()
            .price(DEFAULT_PRICE)
            .synch(DEFAULT_SYNCH)
            .verified(DEFAULT_VERIFIED)
            .idUser(DEFAULT_ID_USER)
            .created(DEFAULT_CREATED)
            .verifiedDate(DEFAULT_VERIFIED_DATE)
            .userKey(DEFAULT_USER_KEY)
            .userName(DEFAULT_USER_NAME)
            .annulled(DEFAULT_ANNULLED);
        return roadmap;
    }

    @Before
    public void initTest() {
        roadmap = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoadmap() throws Exception {
        int databaseSizeBeforeCreate = roadmapRepository.findAll().size();

        // Create the Roadmap
        restRoadmapMockMvc.perform(post("/api/roadmaps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roadmap)))
            .andExpect(status().isCreated());

        // Validate the Roadmap in the database
        List<Roadmap> roadmapList = roadmapRepository.findAll();
        assertThat(roadmapList).hasSize(databaseSizeBeforeCreate + 1);
        Roadmap testRoadmap = roadmapList.get(roadmapList.size() - 1);
        assertThat(testRoadmap.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testRoadmap.isSynch()).isEqualTo(DEFAULT_SYNCH);
        assertThat(testRoadmap.isVerified()).isEqualTo(DEFAULT_VERIFIED);
        assertThat(testRoadmap.getIdUser()).isEqualTo(DEFAULT_ID_USER);
        assertThat(testRoadmap.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testRoadmap.getVerifiedDate()).isEqualTo(DEFAULT_VERIFIED_DATE);
        assertThat(testRoadmap.getUserKey()).isEqualTo(DEFAULT_USER_KEY);
        assertThat(testRoadmap.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testRoadmap.isAnnulled()).isEqualTo(DEFAULT_ANNULLED);
    }

    @Test
    @Transactional
    public void createRoadmapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roadmapRepository.findAll().size();

        // Create the Roadmap with an existing ID
        roadmap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoadmapMockMvc.perform(post("/api/roadmaps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roadmap)))
            .andExpect(status().isBadRequest());

        // Validate the Roadmap in the database
        List<Roadmap> roadmapList = roadmapRepository.findAll();
        assertThat(roadmapList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRoadmaps() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList
        restRoadmapMockMvc.perform(get("/api/roadmaps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roadmap.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].synch").value(hasItem(DEFAULT_SYNCH.booleanValue())))
            .andExpect(jsonPath("$.[*].verified").value(hasItem(DEFAULT_VERIFIED.booleanValue())))
            .andExpect(jsonPath("$.[*].idUser").value(hasItem(DEFAULT_ID_USER.toString())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].verifiedDate").value(hasItem(sameInstant(DEFAULT_VERIFIED_DATE))))
            .andExpect(jsonPath("$.[*].userKey").value(hasItem(DEFAULT_USER_KEY.toString())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].annulled").value(hasItem(DEFAULT_ANNULLED.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getRoadmap() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get the roadmap
        restRoadmapMockMvc.perform(get("/api/roadmaps/{id}", roadmap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(roadmap.getId().intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.synch").value(DEFAULT_SYNCH.booleanValue()))
            .andExpect(jsonPath("$.verified").value(DEFAULT_VERIFIED.booleanValue()))
            .andExpect(jsonPath("$.idUser").value(DEFAULT_ID_USER.toString()))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)))
            .andExpect(jsonPath("$.verifiedDate").value(sameInstant(DEFAULT_VERIFIED_DATE)))
            .andExpect(jsonPath("$.userKey").value(DEFAULT_USER_KEY.toString()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME.toString()))
            .andExpect(jsonPath("$.annulled").value(DEFAULT_ANNULLED.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllRoadmapsByPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where price equals to DEFAULT_PRICE
        defaultRoadmapShouldBeFound("price.equals=" + DEFAULT_PRICE);

        // Get all the roadmapList where price equals to UPDATED_PRICE
        defaultRoadmapShouldNotBeFound("price.equals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByPriceIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where price in DEFAULT_PRICE or UPDATED_PRICE
        defaultRoadmapShouldBeFound("price.in=" + DEFAULT_PRICE + "," + UPDATED_PRICE);

        // Get all the roadmapList where price equals to UPDATED_PRICE
        defaultRoadmapShouldNotBeFound("price.in=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where price is not null
        defaultRoadmapShouldBeFound("price.specified=true");

        // Get all the roadmapList where price is null
        defaultRoadmapShouldNotBeFound("price.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsBySynchIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where synch equals to DEFAULT_SYNCH
        defaultRoadmapShouldBeFound("synch.equals=" + DEFAULT_SYNCH);

        // Get all the roadmapList where synch equals to UPDATED_SYNCH
        defaultRoadmapShouldNotBeFound("synch.equals=" + UPDATED_SYNCH);
    }

    @Test
    @Transactional
    public void getAllRoadmapsBySynchIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where synch in DEFAULT_SYNCH or UPDATED_SYNCH
        defaultRoadmapShouldBeFound("synch.in=" + DEFAULT_SYNCH + "," + UPDATED_SYNCH);

        // Get all the roadmapList where synch equals to UPDATED_SYNCH
        defaultRoadmapShouldNotBeFound("synch.in=" + UPDATED_SYNCH);
    }

    @Test
    @Transactional
    public void getAllRoadmapsBySynchIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where synch is not null
        defaultRoadmapShouldBeFound("synch.specified=true");

        // Get all the roadmapList where synch is null
        defaultRoadmapShouldNotBeFound("synch.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verified equals to DEFAULT_VERIFIED
        defaultRoadmapShouldBeFound("verified.equals=" + DEFAULT_VERIFIED);

        // Get all the roadmapList where verified equals to UPDATED_VERIFIED
        defaultRoadmapShouldNotBeFound("verified.equals=" + UPDATED_VERIFIED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verified in DEFAULT_VERIFIED or UPDATED_VERIFIED
        defaultRoadmapShouldBeFound("verified.in=" + DEFAULT_VERIFIED + "," + UPDATED_VERIFIED);

        // Get all the roadmapList where verified equals to UPDATED_VERIFIED
        defaultRoadmapShouldNotBeFound("verified.in=" + UPDATED_VERIFIED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verified is not null
        defaultRoadmapShouldBeFound("verified.specified=true");

        // Get all the roadmapList where verified is null
        defaultRoadmapShouldNotBeFound("verified.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByIdUserIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where idUser equals to DEFAULT_ID_USER
        defaultRoadmapShouldBeFound("idUser.equals=" + DEFAULT_ID_USER);

        // Get all the roadmapList where idUser equals to UPDATED_ID_USER
        defaultRoadmapShouldNotBeFound("idUser.equals=" + UPDATED_ID_USER);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByIdUserIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where idUser in DEFAULT_ID_USER or UPDATED_ID_USER
        defaultRoadmapShouldBeFound("idUser.in=" + DEFAULT_ID_USER + "," + UPDATED_ID_USER);

        // Get all the roadmapList where idUser equals to UPDATED_ID_USER
        defaultRoadmapShouldNotBeFound("idUser.in=" + UPDATED_ID_USER);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByIdUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where idUser is not null
        defaultRoadmapShouldBeFound("idUser.specified=true");

        // Get all the roadmapList where idUser is null
        defaultRoadmapShouldNotBeFound("idUser.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByCreatedIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where created equals to DEFAULT_CREATED
        defaultRoadmapShouldBeFound("created.equals=" + DEFAULT_CREATED);

        // Get all the roadmapList where created equals to UPDATED_CREATED
        defaultRoadmapShouldNotBeFound("created.equals=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByCreatedIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where created in DEFAULT_CREATED or UPDATED_CREATED
        defaultRoadmapShouldBeFound("created.in=" + DEFAULT_CREATED + "," + UPDATED_CREATED);

        // Get all the roadmapList where created equals to UPDATED_CREATED
        defaultRoadmapShouldNotBeFound("created.in=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByCreatedIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where created is not null
        defaultRoadmapShouldBeFound("created.specified=true");

        // Get all the roadmapList where created is null
        defaultRoadmapShouldNotBeFound("created.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByCreatedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where created greater than or equals to DEFAULT_CREATED
        defaultRoadmapShouldBeFound("created.greaterOrEqualThan=" + DEFAULT_CREATED);

        // Get all the roadmapList where created greater than or equals to UPDATED_CREATED
        defaultRoadmapShouldNotBeFound("created.greaterOrEqualThan=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByCreatedIsLessThanSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where created less than or equals to DEFAULT_CREATED
        defaultRoadmapShouldNotBeFound("created.lessThan=" + DEFAULT_CREATED);

        // Get all the roadmapList where created less than or equals to UPDATED_CREATED
        defaultRoadmapShouldBeFound("created.lessThan=" + UPDATED_CREATED);
    }


    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verifiedDate equals to DEFAULT_VERIFIED_DATE
        defaultRoadmapShouldBeFound("verifiedDate.equals=" + DEFAULT_VERIFIED_DATE);

        // Get all the roadmapList where verifiedDate equals to UPDATED_VERIFIED_DATE
        defaultRoadmapShouldNotBeFound("verifiedDate.equals=" + UPDATED_VERIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verifiedDate in DEFAULT_VERIFIED_DATE or UPDATED_VERIFIED_DATE
        defaultRoadmapShouldBeFound("verifiedDate.in=" + DEFAULT_VERIFIED_DATE + "," + UPDATED_VERIFIED_DATE);

        // Get all the roadmapList where verifiedDate equals to UPDATED_VERIFIED_DATE
        defaultRoadmapShouldNotBeFound("verifiedDate.in=" + UPDATED_VERIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verifiedDate is not null
        defaultRoadmapShouldBeFound("verifiedDate.specified=true");

        // Get all the roadmapList where verifiedDate is null
        defaultRoadmapShouldNotBeFound("verifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verifiedDate greater than or equals to DEFAULT_VERIFIED_DATE
        defaultRoadmapShouldBeFound("verifiedDate.greaterOrEqualThan=" + DEFAULT_VERIFIED_DATE);

        // Get all the roadmapList where verifiedDate greater than or equals to UPDATED_VERIFIED_DATE
        defaultRoadmapShouldNotBeFound("verifiedDate.greaterOrEqualThan=" + UPDATED_VERIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByVerifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where verifiedDate less than or equals to DEFAULT_VERIFIED_DATE
        defaultRoadmapShouldNotBeFound("verifiedDate.lessThan=" + DEFAULT_VERIFIED_DATE);

        // Get all the roadmapList where verifiedDate less than or equals to UPDATED_VERIFIED_DATE
        defaultRoadmapShouldBeFound("verifiedDate.lessThan=" + UPDATED_VERIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllRoadmapsByUserKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userKey equals to DEFAULT_USER_KEY
        defaultRoadmapShouldBeFound("userKey.equals=" + DEFAULT_USER_KEY);

        // Get all the roadmapList where userKey equals to UPDATED_USER_KEY
        defaultRoadmapShouldNotBeFound("userKey.equals=" + UPDATED_USER_KEY);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByUserKeyIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userKey in DEFAULT_USER_KEY or UPDATED_USER_KEY
        defaultRoadmapShouldBeFound("userKey.in=" + DEFAULT_USER_KEY + "," + UPDATED_USER_KEY);

        // Get all the roadmapList where userKey equals to UPDATED_USER_KEY
        defaultRoadmapShouldNotBeFound("userKey.in=" + UPDATED_USER_KEY);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByUserKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userKey is not null
        defaultRoadmapShouldBeFound("userKey.specified=true");

        // Get all the roadmapList where userKey is null
        defaultRoadmapShouldNotBeFound("userKey.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByUserNameIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userName equals to DEFAULT_USER_NAME
        defaultRoadmapShouldBeFound("userName.equals=" + DEFAULT_USER_NAME);

        // Get all the roadmapList where userName equals to UPDATED_USER_NAME
        defaultRoadmapShouldNotBeFound("userName.equals=" + UPDATED_USER_NAME);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByUserNameIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userName in DEFAULT_USER_NAME or UPDATED_USER_NAME
        defaultRoadmapShouldBeFound("userName.in=" + DEFAULT_USER_NAME + "," + UPDATED_USER_NAME);

        // Get all the roadmapList where userName equals to UPDATED_USER_NAME
        defaultRoadmapShouldNotBeFound("userName.in=" + UPDATED_USER_NAME);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByUserNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where userName is not null
        defaultRoadmapShouldBeFound("userName.specified=true");

        // Get all the roadmapList where userName is null
        defaultRoadmapShouldNotBeFound("userName.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByAnnulledIsEqualToSomething() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where annulled equals to DEFAULT_ANNULLED
        defaultRoadmapShouldBeFound("annulled.equals=" + DEFAULT_ANNULLED);

        // Get all the roadmapList where annulled equals to UPDATED_ANNULLED
        defaultRoadmapShouldNotBeFound("annulled.equals=" + UPDATED_ANNULLED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByAnnulledIsInShouldWork() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where annulled in DEFAULT_ANNULLED or UPDATED_ANNULLED
        defaultRoadmapShouldBeFound("annulled.in=" + DEFAULT_ANNULLED + "," + UPDATED_ANNULLED);

        // Get all the roadmapList where annulled equals to UPDATED_ANNULLED
        defaultRoadmapShouldNotBeFound("annulled.in=" + UPDATED_ANNULLED);
    }

    @Test
    @Transactional
    public void getAllRoadmapsByAnnulledIsNullOrNotNull() throws Exception {
        // Initialize the database
        roadmapRepository.saveAndFlush(roadmap);

        // Get all the roadmapList where annulled is not null
        defaultRoadmapShouldBeFound("annulled.specified=true");

        // Get all the roadmapList where annulled is null
        defaultRoadmapShouldNotBeFound("annulled.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoadmapsByRoutersIsEqualToSomething() throws Exception {
        // Initialize the database
        Routers routers = RoutersResourceIntTest.createEntity(em);
        em.persist(routers);
        em.flush();
        roadmap.setRouters(routers);
        roadmapRepository.saveAndFlush(roadmap);
        Long routersId = routers.getId();

        // Get all the roadmapList where routers equals to routersId
        defaultRoadmapShouldBeFound("routersId.equals=" + routersId);

        // Get all the roadmapList where routers equals to routersId + 1
        defaultRoadmapShouldNotBeFound("routersId.equals=" + (routersId + 1));
    }


    @Test
    @Transactional
    public void getAllRoadmapsByVehicleIsEqualToSomething() throws Exception {
        // Initialize the database
        Vehicle vehicle = VehicleResourceIntTest.createEntity(em);
        em.persist(vehicle);
        em.flush();
        roadmap.setVehicle(vehicle);
        roadmapRepository.saveAndFlush(roadmap);
        Long vehicleId = vehicle.getId();

        // Get all the roadmapList where vehicle equals to vehicleId
        defaultRoadmapShouldBeFound("vehicleId.equals=" + vehicleId);

        // Get all the roadmapList where vehicle equals to vehicleId + 1
        defaultRoadmapShouldNotBeFound("vehicleId.equals=" + (vehicleId + 1));
    }


    @Test
    @Transactional
    public void getAllRoadmapsByMaterialQuarryCustomerIsEqualToSomething() throws Exception {
        // Initialize the database
        MaterialQuarryCustomer materialQuarryCustomer = MaterialQuarryCustomerResourceIntTest.createEntity(em);
        em.persist(materialQuarryCustomer);
        em.flush();
        roadmap.setMaterialQuarryCustomer(materialQuarryCustomer);
        roadmapRepository.saveAndFlush(roadmap);
        Long materialQuarryCustomerId = materialQuarryCustomer.getId();

        // Get all the roadmapList where materialQuarryCustomer equals to materialQuarryCustomerId
        defaultRoadmapShouldBeFound("materialQuarryCustomerId.equals=" + materialQuarryCustomerId);

        // Get all the roadmapList where materialQuarryCustomer equals to materialQuarryCustomerId + 1
        defaultRoadmapShouldNotBeFound("materialQuarryCustomerId.equals=" + (materialQuarryCustomerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRoadmapShouldBeFound(String filter) throws Exception {
        restRoadmapMockMvc.perform(get("/api/roadmaps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roadmap.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].synch").value(hasItem(DEFAULT_SYNCH.booleanValue())))
            .andExpect(jsonPath("$.[*].verified").value(hasItem(DEFAULT_VERIFIED.booleanValue())))
            .andExpect(jsonPath("$.[*].idUser").value(hasItem(DEFAULT_ID_USER.toString())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].verifiedDate").value(hasItem(sameInstant(DEFAULT_VERIFIED_DATE))))
            .andExpect(jsonPath("$.[*].userKey").value(hasItem(DEFAULT_USER_KEY.toString())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].annulled").value(hasItem(DEFAULT_ANNULLED.booleanValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRoadmapShouldNotBeFound(String filter) throws Exception {
        restRoadmapMockMvc.perform(get("/api/roadmaps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingRoadmap() throws Exception {
        // Get the roadmap
        restRoadmapMockMvc.perform(get("/api/roadmaps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoadmap() throws Exception {
        // Initialize the database
        roadmapService.save(roadmap);

        int databaseSizeBeforeUpdate = roadmapRepository.findAll().size();

        // Update the roadmap
        Roadmap updatedRoadmap = roadmapRepository.findById(roadmap.getId()).get();
        // Disconnect from session so that the updates on updatedRoadmap are not directly saved in db
        em.detach(updatedRoadmap);
        updatedRoadmap
            .price(UPDATED_PRICE)
            .synch(UPDATED_SYNCH)
            .verified(UPDATED_VERIFIED)
            .idUser(UPDATED_ID_USER)
            .created(UPDATED_CREATED)
            .verifiedDate(UPDATED_VERIFIED_DATE)
            .userKey(UPDATED_USER_KEY)
            .userName(UPDATED_USER_NAME)
            .annulled(UPDATED_ANNULLED);

        restRoadmapMockMvc.perform(put("/api/roadmaps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRoadmap)))
            .andExpect(status().isOk());

        // Validate the Roadmap in the database
        List<Roadmap> roadmapList = roadmapRepository.findAll();
        assertThat(roadmapList).hasSize(databaseSizeBeforeUpdate);
        Roadmap testRoadmap = roadmapList.get(roadmapList.size() - 1);
        assertThat(testRoadmap.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testRoadmap.isSynch()).isEqualTo(UPDATED_SYNCH);
        assertThat(testRoadmap.isVerified()).isEqualTo(UPDATED_VERIFIED);
        assertThat(testRoadmap.getIdUser()).isEqualTo(UPDATED_ID_USER);
        assertThat(testRoadmap.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testRoadmap.getVerifiedDate()).isEqualTo(UPDATED_VERIFIED_DATE);
        assertThat(testRoadmap.getUserKey()).isEqualTo(UPDATED_USER_KEY);
        assertThat(testRoadmap.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testRoadmap.isAnnulled()).isEqualTo(UPDATED_ANNULLED);
    }

    @Test
    @Transactional
    public void updateNonExistingRoadmap() throws Exception {
        int databaseSizeBeforeUpdate = roadmapRepository.findAll().size();

        // Create the Roadmap

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRoadmapMockMvc.perform(put("/api/roadmaps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roadmap)))
            .andExpect(status().isBadRequest());

        // Validate the Roadmap in the database
        List<Roadmap> roadmapList = roadmapRepository.findAll();
        assertThat(roadmapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRoadmap() throws Exception {
        // Initialize the database
        roadmapService.save(roadmap);

        int databaseSizeBeforeDelete = roadmapRepository.findAll().size();

        // Get the roadmap
        restRoadmapMockMvc.perform(delete("/api/roadmaps/{id}", roadmap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Roadmap> roadmapList = roadmapRepository.findAll();
        assertThat(roadmapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Roadmap.class);
        Roadmap roadmap1 = new Roadmap();
        roadmap1.setId(1L);
        Roadmap roadmap2 = new Roadmap();
        roadmap2.setId(roadmap1.getId());
        assertThat(roadmap1).isEqualTo(roadmap2);
        roadmap2.setId(2L);
        assertThat(roadmap1).isNotEqualTo(roadmap2);
        roadmap1.setId(null);
        assertThat(roadmap1).isNotEqualTo(roadmap2);
    }
}
