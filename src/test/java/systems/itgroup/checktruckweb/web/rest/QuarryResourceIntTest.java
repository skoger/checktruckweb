package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Quarry;
import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.repository.QuarryRepository;
import systems.itgroup.checktruckweb.service.QuarryService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.QuarryCriteria;
import systems.itgroup.checktruckweb.service.QuarryQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QuarryResource REST controller.
 *
 * @see QuarryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class QuarryResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private QuarryRepository quarryRepository;

    

    @Autowired
    private QuarryService quarryService;

    @Autowired
    private QuarryQueryService quarryQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restQuarryMockMvc;

    private Quarry quarry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final QuarryResource quarryResource = new QuarryResource(quarryService, quarryQueryService);
        this.restQuarryMockMvc = MockMvcBuilders.standaloneSetup(quarryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Quarry createEntity(EntityManager em) {
        Quarry quarry = new Quarry()
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        MaterialQuarryCustomer materialQuarryCustomer = MaterialQuarryCustomerResourceIntTest.createEntity(em);
        em.persist(materialQuarryCustomer);
        em.flush();
        quarry.getIdquarries().add(materialQuarryCustomer);
        return quarry;
    }

    @Before
    public void initTest() {
        quarry = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuarry() throws Exception {
        int databaseSizeBeforeCreate = quarryRepository.findAll().size();

        // Create the Quarry
        restQuarryMockMvc.perform(post("/api/quarries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(quarry)))
            .andExpect(status().isCreated());

        // Validate the Quarry in the database
        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeCreate + 1);
        Quarry testQuarry = quarryList.get(quarryList.size() - 1);
        assertThat(testQuarry.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createQuarryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = quarryRepository.findAll().size();

        // Create the Quarry with an existing ID
        quarry.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuarryMockMvc.perform(post("/api/quarries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(quarry)))
            .andExpect(status().isBadRequest());

        // Validate the Quarry in the database
        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = quarryRepository.findAll().size();
        // set the field null
        quarry.setDescription(null);

        // Create the Quarry, which fails.

        restQuarryMockMvc.perform(post("/api/quarries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(quarry)))
            .andExpect(status().isBadRequest());

        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQuarries() throws Exception {
        // Initialize the database
        quarryRepository.saveAndFlush(quarry);

        // Get all the quarryList
        restQuarryMockMvc.perform(get("/api/quarries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quarry.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    

    @Test
    @Transactional
    public void getQuarry() throws Exception {
        // Initialize the database
        quarryRepository.saveAndFlush(quarry);

        // Get the quarry
        restQuarryMockMvc.perform(get("/api/quarries/{id}", quarry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(quarry.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllQuarriesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        quarryRepository.saveAndFlush(quarry);

        // Get all the quarryList where description equals to DEFAULT_DESCRIPTION
        defaultQuarryShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the quarryList where description equals to UPDATED_DESCRIPTION
        defaultQuarryShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllQuarriesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        quarryRepository.saveAndFlush(quarry);

        // Get all the quarryList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultQuarryShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the quarryList where description equals to UPDATED_DESCRIPTION
        defaultQuarryShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllQuarriesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        quarryRepository.saveAndFlush(quarry);

        // Get all the quarryList where description is not null
        defaultQuarryShouldBeFound("description.specified=true");

        // Get all the quarryList where description is null
        defaultQuarryShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuarriesByIdquarryIsEqualToSomething() throws Exception {
        // Initialize the database
        MaterialQuarryCustomer idquarry = MaterialQuarryCustomerResourceIntTest.createEntity(em);
        em.persist(idquarry);
        em.flush();
        quarry.addIdquarry(idquarry);
        quarryRepository.saveAndFlush(quarry);
        Long idquarryId = idquarry.getId();

        // Get all the quarryList where idquarry equals to idquarryId
        defaultQuarryShouldBeFound("idquarryId.equals=" + idquarryId);

        // Get all the quarryList where idquarry equals to idquarryId + 1
        defaultQuarryShouldNotBeFound("idquarryId.equals=" + (idquarryId + 1));
    }


    @Test
    @Transactional
    public void getAllQuarriesByVehicleIsEqualToSomething() throws Exception {
        // Initialize the database
        Vehicle vehicle = VehicleResourceIntTest.createEntity(em);
        em.persist(vehicle);
        em.flush();
        quarry.addVehicle(vehicle);
        quarryRepository.saveAndFlush(quarry);
        Long vehicleId = vehicle.getId();

        // Get all the quarryList where vehicle equals to vehicleId
        defaultQuarryShouldBeFound("vehicleId.equals=" + vehicleId);

        // Get all the quarryList where vehicle equals to vehicleId + 1
        defaultQuarryShouldNotBeFound("vehicleId.equals=" + (vehicleId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultQuarryShouldBeFound(String filter) throws Exception {
        restQuarryMockMvc.perform(get("/api/quarries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quarry.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultQuarryShouldNotBeFound(String filter) throws Exception {
        restQuarryMockMvc.perform(get("/api/quarries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingQuarry() throws Exception {
        // Get the quarry
        restQuarryMockMvc.perform(get("/api/quarries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuarry() throws Exception {
        // Initialize the database
        quarryService.save(quarry);

        int databaseSizeBeforeUpdate = quarryRepository.findAll().size();

        // Update the quarry
        Quarry updatedQuarry = quarryRepository.findById(quarry.getId()).get();
        // Disconnect from session so that the updates on updatedQuarry are not directly saved in db
        em.detach(updatedQuarry);
        updatedQuarry
            .description(UPDATED_DESCRIPTION);

        restQuarryMockMvc.perform(put("/api/quarries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQuarry)))
            .andExpect(status().isOk());

        // Validate the Quarry in the database
        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeUpdate);
        Quarry testQuarry = quarryList.get(quarryList.size() - 1);
        assertThat(testQuarry.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingQuarry() throws Exception {
        int databaseSizeBeforeUpdate = quarryRepository.findAll().size();

        // Create the Quarry

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restQuarryMockMvc.perform(put("/api/quarries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(quarry)))
            .andExpect(status().isBadRequest());

        // Validate the Quarry in the database
        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuarry() throws Exception {
        // Initialize the database
        quarryService.save(quarry);

        int databaseSizeBeforeDelete = quarryRepository.findAll().size();

        // Get the quarry
        restQuarryMockMvc.perform(delete("/api/quarries/{id}", quarry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Quarry> quarryList = quarryRepository.findAll();
        assertThat(quarryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Quarry.class);
        Quarry quarry1 = new Quarry();
        quarry1.setId(1L);
        Quarry quarry2 = new Quarry();
        quarry2.setId(quarry1.getId());
        assertThat(quarry1).isEqualTo(quarry2);
        quarry2.setId(2L);
        assertThat(quarry1).isNotEqualTo(quarry2);
        quarry1.setId(null);
        assertThat(quarry1).isNotEqualTo(quarry2);
    }
}
