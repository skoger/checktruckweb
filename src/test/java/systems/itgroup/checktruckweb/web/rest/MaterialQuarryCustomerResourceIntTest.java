package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.domain.Customer;
import systems.itgroup.checktruckweb.domain.Quarry;
import systems.itgroup.checktruckweb.domain.Material;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.repository.MaterialQuarryCustomerRepository;
import systems.itgroup.checktruckweb.service.MaterialQuarryCustomerService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.MaterialQuarryCustomerCriteria;
import systems.itgroup.checktruckweb.service.MaterialQuarryCustomerQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MaterialQuarryCustomerResource REST controller.
 *
 * @see MaterialQuarryCustomerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class MaterialQuarryCustomerResourceIntTest {

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    @Autowired
    private MaterialQuarryCustomerRepository materialQuarryCustomerRepository;

    

    @Autowired
    private MaterialQuarryCustomerService materialQuarryCustomerService;

    @Autowired
    private MaterialQuarryCustomerQueryService materialQuarryCustomerQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMaterialQuarryCustomerMockMvc;

    private MaterialQuarryCustomer materialQuarryCustomer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialQuarryCustomerResource materialQuarryCustomerResource = new MaterialQuarryCustomerResource(materialQuarryCustomerService, materialQuarryCustomerQueryService);
        this.restMaterialQuarryCustomerMockMvc = MockMvcBuilders.standaloneSetup(materialQuarryCustomerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialQuarryCustomer createEntity(EntityManager em) {
        MaterialQuarryCustomer materialQuarryCustomer = new MaterialQuarryCustomer()
            .price(DEFAULT_PRICE);
        return materialQuarryCustomer;
    }

    @Before
    public void initTest() {
        materialQuarryCustomer = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialQuarryCustomer() throws Exception {
        int databaseSizeBeforeCreate = materialQuarryCustomerRepository.findAll().size();

        // Create the MaterialQuarryCustomer
        restMaterialQuarryCustomerMockMvc.perform(post("/api/material-quarry-customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuarryCustomer)))
            .andExpect(status().isCreated());

        // Validate the MaterialQuarryCustomer in the database
        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialQuarryCustomer testMaterialQuarryCustomer = materialQuarryCustomerList.get(materialQuarryCustomerList.size() - 1);
        assertThat(testMaterialQuarryCustomer.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createMaterialQuarryCustomerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialQuarryCustomerRepository.findAll().size();

        // Create the MaterialQuarryCustomer with an existing ID
        materialQuarryCustomer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialQuarryCustomerMockMvc.perform(post("/api/material-quarry-customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuarryCustomer)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialQuarryCustomer in the database
        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = materialQuarryCustomerRepository.findAll().size();
        // set the field null
        materialQuarryCustomer.setPrice(null);

        // Create the MaterialQuarryCustomer, which fails.

        restMaterialQuarryCustomerMockMvc.perform(post("/api/material-quarry-customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuarryCustomer)))
            .andExpect(status().isBadRequest());

        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMaterialQuarryCustomers() throws Exception {
        // Initialize the database
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);

        // Get all the materialQuarryCustomerList
        restMaterialQuarryCustomerMockMvc.perform(get("/api/material-quarry-customers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialQuarryCustomer.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }
    

    @Test
    @Transactional
    public void getMaterialQuarryCustomer() throws Exception {
        // Initialize the database
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);

        // Get the materialQuarryCustomer
        restMaterialQuarryCustomerMockMvc.perform(get("/api/material-quarry-customers/{id}", materialQuarryCustomer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialQuarryCustomer.getId().intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);

        // Get all the materialQuarryCustomerList where price equals to DEFAULT_PRICE
        defaultMaterialQuarryCustomerShouldBeFound("price.equals=" + DEFAULT_PRICE);

        // Get all the materialQuarryCustomerList where price equals to UPDATED_PRICE
        defaultMaterialQuarryCustomerShouldNotBeFound("price.equals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByPriceIsInShouldWork() throws Exception {
        // Initialize the database
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);

        // Get all the materialQuarryCustomerList where price in DEFAULT_PRICE or UPDATED_PRICE
        defaultMaterialQuarryCustomerShouldBeFound("price.in=" + DEFAULT_PRICE + "," + UPDATED_PRICE);

        // Get all the materialQuarryCustomerList where price equals to UPDATED_PRICE
        defaultMaterialQuarryCustomerShouldNotBeFound("price.in=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);

        // Get all the materialQuarryCustomerList where price is not null
        defaultMaterialQuarryCustomerShouldBeFound("price.specified=true");

        // Get all the materialQuarryCustomerList where price is null
        defaultMaterialQuarryCustomerShouldNotBeFound("price.specified=false");
    }

    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByCustomerIsEqualToSomething() throws Exception {
        // Initialize the database
        Customer customer = CustomerResourceIntTest.createEntity(em);
        em.persist(customer);
        em.flush();
        materialQuarryCustomer.setCustomer(customer);
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);
        Long customerId = customer.getId();

        // Get all the materialQuarryCustomerList where customer equals to customerId
        defaultMaterialQuarryCustomerShouldBeFound("customerId.equals=" + customerId);

        // Get all the materialQuarryCustomerList where customer equals to customerId + 1
        defaultMaterialQuarryCustomerShouldNotBeFound("customerId.equals=" + (customerId + 1));
    }


    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByQuarryIsEqualToSomething() throws Exception {
        // Initialize the database
        Quarry quarry = QuarryResourceIntTest.createEntity(em);
        em.persist(quarry);
        em.flush();
        materialQuarryCustomer.setQuarry(quarry);
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);
        Long quarryId = quarry.getId();

        // Get all the materialQuarryCustomerList where quarry equals to quarryId
        defaultMaterialQuarryCustomerShouldBeFound("quarryId.equals=" + quarryId);

        // Get all the materialQuarryCustomerList where quarry equals to quarryId + 1
        defaultMaterialQuarryCustomerShouldNotBeFound("quarryId.equals=" + (quarryId + 1));
    }


    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByMaterialIsEqualToSomething() throws Exception {
        // Initialize the database
        Material material = MaterialResourceIntTest.createEntity(em);
        em.persist(material);
        em.flush();
        materialQuarryCustomer.setMaterial(material);
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);
        Long materialId = material.getId();

        // Get all the materialQuarryCustomerList where material equals to materialId
        defaultMaterialQuarryCustomerShouldBeFound("materialId.equals=" + materialId);

        // Get all the materialQuarryCustomerList where material equals to materialId + 1
        defaultMaterialQuarryCustomerShouldNotBeFound("materialId.equals=" + (materialId + 1));
    }


    @Test
    @Transactional
    public void getAllMaterialQuarryCustomersByIdmaterialquarryIsEqualToSomething() throws Exception {
        // Initialize the database
        Roadmap idmaterialquarry = RoadmapResourceIntTest.createEntity(em);
        em.persist(idmaterialquarry);
        em.flush();
        materialQuarryCustomer.addIdmaterialquarry(idmaterialquarry);
        materialQuarryCustomerRepository.saveAndFlush(materialQuarryCustomer);
        Long idmaterialquarryId = idmaterialquarry.getId();

        // Get all the materialQuarryCustomerList where idmaterialquarry equals to idmaterialquarryId
        defaultMaterialQuarryCustomerShouldBeFound("idmaterialquarryId.equals=" + idmaterialquarryId);

        // Get all the materialQuarryCustomerList where idmaterialquarry equals to idmaterialquarryId + 1
        defaultMaterialQuarryCustomerShouldNotBeFound("idmaterialquarryId.equals=" + (idmaterialquarryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMaterialQuarryCustomerShouldBeFound(String filter) throws Exception {
        restMaterialQuarryCustomerMockMvc.perform(get("/api/material-quarry-customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialQuarryCustomer.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMaterialQuarryCustomerShouldNotBeFound(String filter) throws Exception {
        restMaterialQuarryCustomerMockMvc.perform(get("/api/material-quarry-customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingMaterialQuarryCustomer() throws Exception {
        // Get the materialQuarryCustomer
        restMaterialQuarryCustomerMockMvc.perform(get("/api/material-quarry-customers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialQuarryCustomer() throws Exception {
        // Initialize the database
        materialQuarryCustomerService.save(materialQuarryCustomer);

        int databaseSizeBeforeUpdate = materialQuarryCustomerRepository.findAll().size();

        // Update the materialQuarryCustomer
        MaterialQuarryCustomer updatedMaterialQuarryCustomer = materialQuarryCustomerRepository.findById(materialQuarryCustomer.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialQuarryCustomer are not directly saved in db
        em.detach(updatedMaterialQuarryCustomer);
        updatedMaterialQuarryCustomer
            .price(UPDATED_PRICE);

        restMaterialQuarryCustomerMockMvc.perform(put("/api/material-quarry-customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialQuarryCustomer)))
            .andExpect(status().isOk());

        // Validate the MaterialQuarryCustomer in the database
        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeUpdate);
        MaterialQuarryCustomer testMaterialQuarryCustomer = materialQuarryCustomerList.get(materialQuarryCustomerList.size() - 1);
        assertThat(testMaterialQuarryCustomer.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialQuarryCustomer() throws Exception {
        int databaseSizeBeforeUpdate = materialQuarryCustomerRepository.findAll().size();

        // Create the MaterialQuarryCustomer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMaterialQuarryCustomerMockMvc.perform(put("/api/material-quarry-customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuarryCustomer)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialQuarryCustomer in the database
        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialQuarryCustomer() throws Exception {
        // Initialize the database
        materialQuarryCustomerService.save(materialQuarryCustomer);

        int databaseSizeBeforeDelete = materialQuarryCustomerRepository.findAll().size();

        // Get the materialQuarryCustomer
        restMaterialQuarryCustomerMockMvc.perform(delete("/api/material-quarry-customers/{id}", materialQuarryCustomer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MaterialQuarryCustomer> materialQuarryCustomerList = materialQuarryCustomerRepository.findAll();
        assertThat(materialQuarryCustomerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialQuarryCustomer.class);
        MaterialQuarryCustomer materialQuarryCustomer1 = new MaterialQuarryCustomer();
        materialQuarryCustomer1.setId(1L);
        MaterialQuarryCustomer materialQuarryCustomer2 = new MaterialQuarryCustomer();
        materialQuarryCustomer2.setId(materialQuarryCustomer1.getId());
        assertThat(materialQuarryCustomer1).isEqualTo(materialQuarryCustomer2);
        materialQuarryCustomer2.setId(2L);
        assertThat(materialQuarryCustomer1).isNotEqualTo(materialQuarryCustomer2);
        materialQuarryCustomer1.setId(null);
        assertThat(materialQuarryCustomer1).isNotEqualTo(materialQuarryCustomer2);
    }
}
