package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Capacity;
import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.repository.CapacityRepository;
import systems.itgroup.checktruckweb.service.CapacityService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.CapacityCriteria;
import systems.itgroup.checktruckweb.service.CapacityQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CapacityResource REST controller.
 *
 * @see CapacityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class CapacityResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CapacityRepository capacityRepository;

    

    @Autowired
    private CapacityService capacityService;

    @Autowired
    private CapacityQueryService capacityQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCapacityMockMvc;

    private Capacity capacity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CapacityResource capacityResource = new CapacityResource(capacityService, capacityQueryService);
        this.restCapacityMockMvc = MockMvcBuilders.standaloneSetup(capacityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Capacity createEntity(EntityManager em) {
        Capacity capacity = new Capacity()
            .description(DEFAULT_DESCRIPTION);
        return capacity;
    }

    @Before
    public void initTest() {
        capacity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCapacity() throws Exception {
        int databaseSizeBeforeCreate = capacityRepository.findAll().size();

        // Create the Capacity
        restCapacityMockMvc.perform(post("/api/capacities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(capacity)))
            .andExpect(status().isCreated());

        // Validate the Capacity in the database
        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeCreate + 1);
        Capacity testCapacity = capacityList.get(capacityList.size() - 1);
        assertThat(testCapacity.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCapacityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = capacityRepository.findAll().size();

        // Create the Capacity with an existing ID
        capacity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCapacityMockMvc.perform(post("/api/capacities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(capacity)))
            .andExpect(status().isBadRequest());

        // Validate the Capacity in the database
        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = capacityRepository.findAll().size();
        // set the field null
        capacity.setDescription(null);

        // Create the Capacity, which fails.

        restCapacityMockMvc.perform(post("/api/capacities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(capacity)))
            .andExpect(status().isBadRequest());

        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCapacities() throws Exception {
        // Initialize the database
        capacityRepository.saveAndFlush(capacity);

        // Get all the capacityList
        restCapacityMockMvc.perform(get("/api/capacities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(capacity.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    

    @Test
    @Transactional
    public void getCapacity() throws Exception {
        // Initialize the database
        capacityRepository.saveAndFlush(capacity);

        // Get the capacity
        restCapacityMockMvc.perform(get("/api/capacities/{id}", capacity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(capacity.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllCapacitiesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        capacityRepository.saveAndFlush(capacity);

        // Get all the capacityList where description equals to DEFAULT_DESCRIPTION
        defaultCapacityShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the capacityList where description equals to UPDATED_DESCRIPTION
        defaultCapacityShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCapacitiesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        capacityRepository.saveAndFlush(capacity);

        // Get all the capacityList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultCapacityShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the capacityList where description equals to UPDATED_DESCRIPTION
        defaultCapacityShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCapacitiesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        capacityRepository.saveAndFlush(capacity);

        // Get all the capacityList where description is not null
        defaultCapacityShouldBeFound("description.specified=true");

        // Get all the capacityList where description is null
        defaultCapacityShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllCapacitiesByIdcapacityIsEqualToSomething() throws Exception {
        // Initialize the database
        Vehicle idcapacity = VehicleResourceIntTest.createEntity(em);
        em.persist(idcapacity);
        em.flush();
        capacity.addIdcapacity(idcapacity);
        capacityRepository.saveAndFlush(capacity);
        Long idcapacityId = idcapacity.getId();

        // Get all the capacityList where idcapacity equals to idcapacityId
        defaultCapacityShouldBeFound("idcapacityId.equals=" + idcapacityId);

        // Get all the capacityList where idcapacity equals to idcapacityId + 1
        defaultCapacityShouldNotBeFound("idcapacityId.equals=" + (idcapacityId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCapacityShouldBeFound(String filter) throws Exception {
        restCapacityMockMvc.perform(get("/api/capacities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(capacity.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCapacityShouldNotBeFound(String filter) throws Exception {
        restCapacityMockMvc.perform(get("/api/capacities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingCapacity() throws Exception {
        // Get the capacity
        restCapacityMockMvc.perform(get("/api/capacities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCapacity() throws Exception {
        // Initialize the database
        capacityService.save(capacity);

        int databaseSizeBeforeUpdate = capacityRepository.findAll().size();

        // Update the capacity
        Capacity updatedCapacity = capacityRepository.findById(capacity.getId()).get();
        // Disconnect from session so that the updates on updatedCapacity are not directly saved in db
        em.detach(updatedCapacity);
        updatedCapacity
            .description(UPDATED_DESCRIPTION);

        restCapacityMockMvc.perform(put("/api/capacities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCapacity)))
            .andExpect(status().isOk());

        // Validate the Capacity in the database
        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeUpdate);
        Capacity testCapacity = capacityList.get(capacityList.size() - 1);
        assertThat(testCapacity.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCapacity() throws Exception {
        int databaseSizeBeforeUpdate = capacityRepository.findAll().size();

        // Create the Capacity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCapacityMockMvc.perform(put("/api/capacities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(capacity)))
            .andExpect(status().isBadRequest());

        // Validate the Capacity in the database
        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCapacity() throws Exception {
        // Initialize the database
        capacityService.save(capacity);

        int databaseSizeBeforeDelete = capacityRepository.findAll().size();

        // Get the capacity
        restCapacityMockMvc.perform(delete("/api/capacities/{id}", capacity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Capacity> capacityList = capacityRepository.findAll();
        assertThat(capacityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Capacity.class);
        Capacity capacity1 = new Capacity();
        capacity1.setId(1L);
        Capacity capacity2 = new Capacity();
        capacity2.setId(capacity1.getId());
        assertThat(capacity1).isEqualTo(capacity2);
        capacity2.setId(2L);
        assertThat(capacity1).isNotEqualTo(capacity2);
        capacity1.setId(null);
        assertThat(capacity1).isNotEqualTo(capacity2);
    }
}
