package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Carrier;
import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.repository.CarrierRepository;
import systems.itgroup.checktruckweb.service.CarrierService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.CarrierCriteria;
import systems.itgroup.checktruckweb.service.CarrierQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarrierResource REST controller.
 *
 * @see CarrierResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class CarrierResourceIntTest {

    private static final String DEFAULT_BUSINESS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PHONE = "BBBBBBBBBB";

    @Autowired
    private CarrierRepository carrierRepository;

    

    @Autowired
    private CarrierService carrierService;

    @Autowired
    private CarrierQueryService carrierQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCarrierMockMvc;

    private Carrier carrier;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CarrierResource carrierResource = new CarrierResource(carrierService, carrierQueryService);
        this.restCarrierMockMvc = MockMvcBuilders.standaloneSetup(carrierResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Carrier createEntity(EntityManager em) {
        Carrier carrier = new Carrier()
            .businessName(DEFAULT_BUSINESS_NAME)
            .address(DEFAULT_ADDRESS)
            .phone(DEFAULT_PHONE)
            .contact(DEFAULT_CONTACT)
            .contactPhone(DEFAULT_CONTACT_PHONE);
        return carrier;
    }

    @Before
    public void initTest() {
        carrier = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarrier() throws Exception {
        int databaseSizeBeforeCreate = carrierRepository.findAll().size();

        // Create the Carrier
        restCarrierMockMvc.perform(post("/api/carriers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carrier)))
            .andExpect(status().isCreated());

        // Validate the Carrier in the database
        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeCreate + 1);
        Carrier testCarrier = carrierList.get(carrierList.size() - 1);
        assertThat(testCarrier.getBusinessName()).isEqualTo(DEFAULT_BUSINESS_NAME);
        assertThat(testCarrier.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testCarrier.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testCarrier.getContact()).isEqualTo(DEFAULT_CONTACT);
        assertThat(testCarrier.getContactPhone()).isEqualTo(DEFAULT_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void createCarrierWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carrierRepository.findAll().size();

        // Create the Carrier with an existing ID
        carrier.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarrierMockMvc.perform(post("/api/carriers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carrier)))
            .andExpect(status().isBadRequest());

        // Validate the Carrier in the database
        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkBusinessNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = carrierRepository.findAll().size();
        // set the field null
        carrier.setBusinessName(null);

        // Create the Carrier, which fails.

        restCarrierMockMvc.perform(post("/api/carriers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carrier)))
            .andExpect(status().isBadRequest());

        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarriers() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList
        restCarrierMockMvc.perform(get("/api/carriers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carrier.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessName").value(hasItem(DEFAULT_BUSINESS_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.toString())))
            .andExpect(jsonPath("$.[*].contactPhone").value(hasItem(DEFAULT_CONTACT_PHONE.toString())));
    }
    

    @Test
    @Transactional
    public void getCarrier() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get the carrier
        restCarrierMockMvc.perform(get("/api/carriers/{id}", carrier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carrier.getId().intValue()))
            .andExpect(jsonPath("$.businessName").value(DEFAULT_BUSINESS_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.contact").value(DEFAULT_CONTACT.toString()))
            .andExpect(jsonPath("$.contactPhone").value(DEFAULT_CONTACT_PHONE.toString()));
    }

    @Test
    @Transactional
    public void getAllCarriersByBusinessNameIsEqualToSomething() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where businessName equals to DEFAULT_BUSINESS_NAME
        defaultCarrierShouldBeFound("businessName.equals=" + DEFAULT_BUSINESS_NAME);

        // Get all the carrierList where businessName equals to UPDATED_BUSINESS_NAME
        defaultCarrierShouldNotBeFound("businessName.equals=" + UPDATED_BUSINESS_NAME);
    }

    @Test
    @Transactional
    public void getAllCarriersByBusinessNameIsInShouldWork() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where businessName in DEFAULT_BUSINESS_NAME or UPDATED_BUSINESS_NAME
        defaultCarrierShouldBeFound("businessName.in=" + DEFAULT_BUSINESS_NAME + "," + UPDATED_BUSINESS_NAME);

        // Get all the carrierList where businessName equals to UPDATED_BUSINESS_NAME
        defaultCarrierShouldNotBeFound("businessName.in=" + UPDATED_BUSINESS_NAME);
    }

    @Test
    @Transactional
    public void getAllCarriersByBusinessNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where businessName is not null
        defaultCarrierShouldBeFound("businessName.specified=true");

        // Get all the carrierList where businessName is null
        defaultCarrierShouldNotBeFound("businessName.specified=false");
    }

    @Test
    @Transactional
    public void getAllCarriersByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where address equals to DEFAULT_ADDRESS
        defaultCarrierShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the carrierList where address equals to UPDATED_ADDRESS
        defaultCarrierShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCarriersByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultCarrierShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the carrierList where address equals to UPDATED_ADDRESS
        defaultCarrierShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCarriersByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where address is not null
        defaultCarrierShouldBeFound("address.specified=true");

        // Get all the carrierList where address is null
        defaultCarrierShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    public void getAllCarriersByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where phone equals to DEFAULT_PHONE
        defaultCarrierShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the carrierList where phone equals to UPDATED_PHONE
        defaultCarrierShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllCarriersByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultCarrierShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the carrierList where phone equals to UPDATED_PHONE
        defaultCarrierShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllCarriersByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where phone is not null
        defaultCarrierShouldBeFound("phone.specified=true");

        // Get all the carrierList where phone is null
        defaultCarrierShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllCarriersByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contact equals to DEFAULT_CONTACT
        defaultCarrierShouldBeFound("contact.equals=" + DEFAULT_CONTACT);

        // Get all the carrierList where contact equals to UPDATED_CONTACT
        defaultCarrierShouldNotBeFound("contact.equals=" + UPDATED_CONTACT);
    }

    @Test
    @Transactional
    public void getAllCarriersByContactIsInShouldWork() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contact in DEFAULT_CONTACT or UPDATED_CONTACT
        defaultCarrierShouldBeFound("contact.in=" + DEFAULT_CONTACT + "," + UPDATED_CONTACT);

        // Get all the carrierList where contact equals to UPDATED_CONTACT
        defaultCarrierShouldNotBeFound("contact.in=" + UPDATED_CONTACT);
    }

    @Test
    @Transactional
    public void getAllCarriersByContactIsNullOrNotNull() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contact is not null
        defaultCarrierShouldBeFound("contact.specified=true");

        // Get all the carrierList where contact is null
        defaultCarrierShouldNotBeFound("contact.specified=false");
    }

    @Test
    @Transactional
    public void getAllCarriersByContactPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contactPhone equals to DEFAULT_CONTACT_PHONE
        defaultCarrierShouldBeFound("contactPhone.equals=" + DEFAULT_CONTACT_PHONE);

        // Get all the carrierList where contactPhone equals to UPDATED_CONTACT_PHONE
        defaultCarrierShouldNotBeFound("contactPhone.equals=" + UPDATED_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void getAllCarriersByContactPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contactPhone in DEFAULT_CONTACT_PHONE or UPDATED_CONTACT_PHONE
        defaultCarrierShouldBeFound("contactPhone.in=" + DEFAULT_CONTACT_PHONE + "," + UPDATED_CONTACT_PHONE);

        // Get all the carrierList where contactPhone equals to UPDATED_CONTACT_PHONE
        defaultCarrierShouldNotBeFound("contactPhone.in=" + UPDATED_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void getAllCarriersByContactPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        carrierRepository.saveAndFlush(carrier);

        // Get all the carrierList where contactPhone is not null
        defaultCarrierShouldBeFound("contactPhone.specified=true");

        // Get all the carrierList where contactPhone is null
        defaultCarrierShouldNotBeFound("contactPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllCarriersByIdcarrierIsEqualToSomething() throws Exception {
        // Initialize the database
        Vehicle idcarrier = VehicleResourceIntTest.createEntity(em);
        em.persist(idcarrier);
        em.flush();
        carrier.addIdcarrier(idcarrier);
        carrierRepository.saveAndFlush(carrier);
        Long idcarrierId = idcarrier.getId();

        // Get all the carrierList where idcarrier equals to idcarrierId
        defaultCarrierShouldBeFound("idcarrierId.equals=" + idcarrierId);

        // Get all the carrierList where idcarrier equals to idcarrierId + 1
        defaultCarrierShouldNotBeFound("idcarrierId.equals=" + (idcarrierId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCarrierShouldBeFound(String filter) throws Exception {
        restCarrierMockMvc.perform(get("/api/carriers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carrier.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessName").value(hasItem(DEFAULT_BUSINESS_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.toString())))
            .andExpect(jsonPath("$.[*].contactPhone").value(hasItem(DEFAULT_CONTACT_PHONE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCarrierShouldNotBeFound(String filter) throws Exception {
        restCarrierMockMvc.perform(get("/api/carriers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingCarrier() throws Exception {
        // Get the carrier
        restCarrierMockMvc.perform(get("/api/carriers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarrier() throws Exception {
        // Initialize the database
        carrierService.save(carrier);

        int databaseSizeBeforeUpdate = carrierRepository.findAll().size();

        // Update the carrier
        Carrier updatedCarrier = carrierRepository.findById(carrier.getId()).get();
        // Disconnect from session so that the updates on updatedCarrier are not directly saved in db
        em.detach(updatedCarrier);
        updatedCarrier
            .businessName(UPDATED_BUSINESS_NAME)
            .address(UPDATED_ADDRESS)
            .phone(UPDATED_PHONE)
            .contact(UPDATED_CONTACT)
            .contactPhone(UPDATED_CONTACT_PHONE);

        restCarrierMockMvc.perform(put("/api/carriers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCarrier)))
            .andExpect(status().isOk());

        // Validate the Carrier in the database
        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeUpdate);
        Carrier testCarrier = carrierList.get(carrierList.size() - 1);
        assertThat(testCarrier.getBusinessName()).isEqualTo(UPDATED_BUSINESS_NAME);
        assertThat(testCarrier.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCarrier.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testCarrier.getContact()).isEqualTo(UPDATED_CONTACT);
        assertThat(testCarrier.getContactPhone()).isEqualTo(UPDATED_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void updateNonExistingCarrier() throws Exception {
        int databaseSizeBeforeUpdate = carrierRepository.findAll().size();

        // Create the Carrier

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarrierMockMvc.perform(put("/api/carriers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carrier)))
            .andExpect(status().isBadRequest());

        // Validate the Carrier in the database
        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCarrier() throws Exception {
        // Initialize the database
        carrierService.save(carrier);

        int databaseSizeBeforeDelete = carrierRepository.findAll().size();

        // Get the carrier
        restCarrierMockMvc.perform(delete("/api/carriers/{id}", carrier.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Carrier> carrierList = carrierRepository.findAll();
        assertThat(carrierList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Carrier.class);
        Carrier carrier1 = new Carrier();
        carrier1.setId(1L);
        Carrier carrier2 = new Carrier();
        carrier2.setId(carrier1.getId());
        assertThat(carrier1).isEqualTo(carrier2);
        carrier2.setId(2L);
        assertThat(carrier1).isNotEqualTo(carrier2);
        carrier1.setId(null);
        assertThat(carrier1).isNotEqualTo(carrier2);
    }
}
