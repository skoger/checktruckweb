package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Routers;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.repository.RoutersRepository;
import systems.itgroup.checktruckweb.service.RoutersService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.RoutersCriteria;
import systems.itgroup.checktruckweb.service.RoutersQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RoutersResource REST controller.
 *
 * @see RoutersResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class RoutersResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DISTANCE_KM = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISTANCE_KM = new BigDecimal(2);

    private static final BigDecimal DEFAULT_COST = new BigDecimal(1);
    private static final BigDecimal UPDATED_COST = new BigDecimal(2);

    private static final Integer DEFAULT_REPORT_COPIES = 1;
    private static final Integer UPDATED_REPORT_COPIES = 2;

    @Autowired
    private RoutersRepository routersRepository;

    

    @Autowired
    private RoutersService routersService;

    @Autowired
    private RoutersQueryService routersQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRoutersMockMvc;

    private Routers routers;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoutersResource routersResource = new RoutersResource(routersService, routersQueryService);
        this.restRoutersMockMvc = MockMvcBuilders.standaloneSetup(routersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Routers createEntity(EntityManager em) {
        Routers routers = new Routers()
            .description(DEFAULT_DESCRIPTION)
            .distanceKm(DEFAULT_DISTANCE_KM)
            .cost(DEFAULT_COST)
            .reportCopies(DEFAULT_REPORT_COPIES);
        return routers;
    }

    @Before
    public void initTest() {
        routers = createEntity(em);
    }

    @Test
    @Transactional
    public void createRouters() throws Exception {
        int databaseSizeBeforeCreate = routersRepository.findAll().size();

        // Create the Routers
        restRoutersMockMvc.perform(post("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isCreated());

        // Validate the Routers in the database
        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeCreate + 1);
        Routers testRouters = routersList.get(routersList.size() - 1);
        assertThat(testRouters.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRouters.getDistanceKm()).isEqualTo(DEFAULT_DISTANCE_KM);
        assertThat(testRouters.getCost()).isEqualTo(DEFAULT_COST);
        assertThat(testRouters.getReportCopies()).isEqualTo(DEFAULT_REPORT_COPIES);
    }

    @Test
    @Transactional
    public void createRoutersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = routersRepository.findAll().size();

        // Create the Routers with an existing ID
        routers.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoutersMockMvc.perform(post("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isBadRequest());

        // Validate the Routers in the database
        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = routersRepository.findAll().size();
        // set the field null
        routers.setDescription(null);

        // Create the Routers, which fails.

        restRoutersMockMvc.perform(post("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isBadRequest());

        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCostIsRequired() throws Exception {
        int databaseSizeBeforeTest = routersRepository.findAll().size();
        // set the field null
        routers.setCost(null);

        // Create the Routers, which fails.

        restRoutersMockMvc.perform(post("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isBadRequest());

        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReportCopiesIsRequired() throws Exception {
        int databaseSizeBeforeTest = routersRepository.findAll().size();
        // set the field null
        routers.setReportCopies(null);

        // Create the Routers, which fails.

        restRoutersMockMvc.perform(post("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isBadRequest());

        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRouters() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList
        restRoutersMockMvc.perform(get("/api/routers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routers.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].distanceKm").value(hasItem(DEFAULT_DISTANCE_KM.intValue())))
            .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.intValue())))
            .andExpect(jsonPath("$.[*].reportCopies").value(hasItem(DEFAULT_REPORT_COPIES)));
    }
    

    @Test
    @Transactional
    public void getRouters() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get the routers
        restRoutersMockMvc.perform(get("/api/routers/{id}", routers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(routers.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.distanceKm").value(DEFAULT_DISTANCE_KM.intValue()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()))
            .andExpect(jsonPath("$.reportCopies").value(DEFAULT_REPORT_COPIES));
    }

    @Test
    @Transactional
    public void getAllRoutersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where description equals to DEFAULT_DESCRIPTION
        defaultRoutersShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the routersList where description equals to UPDATED_DESCRIPTION
        defaultRoutersShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllRoutersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultRoutersShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the routersList where description equals to UPDATED_DESCRIPTION
        defaultRoutersShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllRoutersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where description is not null
        defaultRoutersShouldBeFound("description.specified=true");

        // Get all the routersList where description is null
        defaultRoutersShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoutersByDistanceKmIsEqualToSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where distanceKm equals to DEFAULT_DISTANCE_KM
        defaultRoutersShouldBeFound("distanceKm.equals=" + DEFAULT_DISTANCE_KM);

        // Get all the routersList where distanceKm equals to UPDATED_DISTANCE_KM
        defaultRoutersShouldNotBeFound("distanceKm.equals=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllRoutersByDistanceKmIsInShouldWork() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where distanceKm in DEFAULT_DISTANCE_KM or UPDATED_DISTANCE_KM
        defaultRoutersShouldBeFound("distanceKm.in=" + DEFAULT_DISTANCE_KM + "," + UPDATED_DISTANCE_KM);

        // Get all the routersList where distanceKm equals to UPDATED_DISTANCE_KM
        defaultRoutersShouldNotBeFound("distanceKm.in=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllRoutersByDistanceKmIsNullOrNotNull() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where distanceKm is not null
        defaultRoutersShouldBeFound("distanceKm.specified=true");

        // Get all the routersList where distanceKm is null
        defaultRoutersShouldNotBeFound("distanceKm.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoutersByCostIsEqualToSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where cost equals to DEFAULT_COST
        defaultRoutersShouldBeFound("cost.equals=" + DEFAULT_COST);

        // Get all the routersList where cost equals to UPDATED_COST
        defaultRoutersShouldNotBeFound("cost.equals=" + UPDATED_COST);
    }

    @Test
    @Transactional
    public void getAllRoutersByCostIsInShouldWork() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where cost in DEFAULT_COST or UPDATED_COST
        defaultRoutersShouldBeFound("cost.in=" + DEFAULT_COST + "," + UPDATED_COST);

        // Get all the routersList where cost equals to UPDATED_COST
        defaultRoutersShouldNotBeFound("cost.in=" + UPDATED_COST);
    }

    @Test
    @Transactional
    public void getAllRoutersByCostIsNullOrNotNull() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where cost is not null
        defaultRoutersShouldBeFound("cost.specified=true");

        // Get all the routersList where cost is null
        defaultRoutersShouldNotBeFound("cost.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoutersByReportCopiesIsEqualToSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where reportCopies equals to DEFAULT_REPORT_COPIES
        defaultRoutersShouldBeFound("reportCopies.equals=" + DEFAULT_REPORT_COPIES);

        // Get all the routersList where reportCopies equals to UPDATED_REPORT_COPIES
        defaultRoutersShouldNotBeFound("reportCopies.equals=" + UPDATED_REPORT_COPIES);
    }

    @Test
    @Transactional
    public void getAllRoutersByReportCopiesIsInShouldWork() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where reportCopies in DEFAULT_REPORT_COPIES or UPDATED_REPORT_COPIES
        defaultRoutersShouldBeFound("reportCopies.in=" + DEFAULT_REPORT_COPIES + "," + UPDATED_REPORT_COPIES);

        // Get all the routersList where reportCopies equals to UPDATED_REPORT_COPIES
        defaultRoutersShouldNotBeFound("reportCopies.in=" + UPDATED_REPORT_COPIES);
    }

    @Test
    @Transactional
    public void getAllRoutersByReportCopiesIsNullOrNotNull() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where reportCopies is not null
        defaultRoutersShouldBeFound("reportCopies.specified=true");

        // Get all the routersList where reportCopies is null
        defaultRoutersShouldNotBeFound("reportCopies.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoutersByReportCopiesIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where reportCopies greater than or equals to DEFAULT_REPORT_COPIES
        defaultRoutersShouldBeFound("reportCopies.greaterOrEqualThan=" + DEFAULT_REPORT_COPIES);

        // Get all the routersList where reportCopies greater than or equals to UPDATED_REPORT_COPIES
        defaultRoutersShouldNotBeFound("reportCopies.greaterOrEqualThan=" + UPDATED_REPORT_COPIES);
    }

    @Test
    @Transactional
    public void getAllRoutersByReportCopiesIsLessThanSomething() throws Exception {
        // Initialize the database
        routersRepository.saveAndFlush(routers);

        // Get all the routersList where reportCopies less than or equals to DEFAULT_REPORT_COPIES
        defaultRoutersShouldNotBeFound("reportCopies.lessThan=" + DEFAULT_REPORT_COPIES);

        // Get all the routersList where reportCopies less than or equals to UPDATED_REPORT_COPIES
        defaultRoutersShouldBeFound("reportCopies.lessThan=" + UPDATED_REPORT_COPIES);
    }


    @Test
    @Transactional
    public void getAllRoutersByRoutersRaodmapIsEqualToSomething() throws Exception {
        // Initialize the database
        Roadmap routersRaodmap = RoadmapResourceIntTest.createEntity(em);
        em.persist(routersRaodmap);
        em.flush();
        routers.addRoutersRaodmap(routersRaodmap);
        routersRepository.saveAndFlush(routers);
        Long routersRaodmapId = routersRaodmap.getId();

        // Get all the routersList where routersRaodmap equals to routersRaodmapId
        defaultRoutersShouldBeFound("routersRaodmapId.equals=" + routersRaodmapId);

        // Get all the routersList where routersRaodmap equals to routersRaodmapId + 1
        defaultRoutersShouldNotBeFound("routersRaodmapId.equals=" + (routersRaodmapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRoutersShouldBeFound(String filter) throws Exception {
        restRoutersMockMvc.perform(get("/api/routers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routers.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].distanceKm").value(hasItem(DEFAULT_DISTANCE_KM.intValue())))
            .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.intValue())))
            .andExpect(jsonPath("$.[*].reportCopies").value(hasItem(DEFAULT_REPORT_COPIES)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRoutersShouldNotBeFound(String filter) throws Exception {
        restRoutersMockMvc.perform(get("/api/routers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingRouters() throws Exception {
        // Get the routers
        restRoutersMockMvc.perform(get("/api/routers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRouters() throws Exception {
        // Initialize the database
        routersService.save(routers);

        int databaseSizeBeforeUpdate = routersRepository.findAll().size();

        // Update the routers
        Routers updatedRouters = routersRepository.findById(routers.getId()).get();
        // Disconnect from session so that the updates on updatedRouters are not directly saved in db
        em.detach(updatedRouters);
        updatedRouters
            .description(UPDATED_DESCRIPTION)
            .distanceKm(UPDATED_DISTANCE_KM)
            .cost(UPDATED_COST)
            .reportCopies(UPDATED_REPORT_COPIES);

        restRoutersMockMvc.perform(put("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRouters)))
            .andExpect(status().isOk());

        // Validate the Routers in the database
        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeUpdate);
        Routers testRouters = routersList.get(routersList.size() - 1);
        assertThat(testRouters.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRouters.getDistanceKm()).isEqualTo(UPDATED_DISTANCE_KM);
        assertThat(testRouters.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testRouters.getReportCopies()).isEqualTo(UPDATED_REPORT_COPIES);
    }

    @Test
    @Transactional
    public void updateNonExistingRouters() throws Exception {
        int databaseSizeBeforeUpdate = routersRepository.findAll().size();

        // Create the Routers

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRoutersMockMvc.perform(put("/api/routers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routers)))
            .andExpect(status().isBadRequest());

        // Validate the Routers in the database
        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRouters() throws Exception {
        // Initialize the database
        routersService.save(routers);

        int databaseSizeBeforeDelete = routersRepository.findAll().size();

        // Get the routers
        restRoutersMockMvc.perform(delete("/api/routers/{id}", routers.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Routers> routersList = routersRepository.findAll();
        assertThat(routersList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Routers.class);
        Routers routers1 = new Routers();
        routers1.setId(1L);
        Routers routers2 = new Routers();
        routers2.setId(routers1.getId());
        assertThat(routers1).isEqualTo(routers2);
        routers2.setId(2L);
        assertThat(routers1).isNotEqualTo(routers2);
        routers1.setId(null);
        assertThat(routers1).isNotEqualTo(routers2);
    }
}
