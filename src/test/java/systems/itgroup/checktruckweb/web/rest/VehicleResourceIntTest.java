package systems.itgroup.checktruckweb.web.rest;

import systems.itgroup.checktruckweb.ChecktruckwebApp;

import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.domain.Carrier;
import systems.itgroup.checktruckweb.domain.Capacity;
import systems.itgroup.checktruckweb.domain.Brand;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.domain.Quarry;
import systems.itgroup.checktruckweb.repository.VehicleRepository;
import systems.itgroup.checktruckweb.service.VehicleService;
import systems.itgroup.checktruckweb.web.rest.errors.ExceptionTranslator;
import systems.itgroup.checktruckweb.service.dto.VehicleCriteria;
import systems.itgroup.checktruckweb.service.VehicleQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static systems.itgroup.checktruckweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VehicleResource REST controller.
 *
 * @see VehicleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChecktruckwebApp.class)
public class VehicleResourceIntTest {

    private static final String DEFAULT_LICENSE_PLATE = "AAAAAAAAAA";
    private static final String UPDATED_LICENSE_PLATE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_QUANTITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_QUANTITY = new BigDecimal(2);

    private static final String DEFAULT_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_MODEL = "BBBBBBBBBB";

    @Autowired
    private VehicleRepository vehicleRepository;

    

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private VehicleQueryService vehicleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVehicleMockMvc;

    private Vehicle vehicle;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VehicleResource vehicleResource = new VehicleResource(vehicleService, vehicleQueryService);
        this.restVehicleMockMvc = MockMvcBuilders.standaloneSetup(vehicleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vehicle createEntity(EntityManager em) {
        Vehicle vehicle = new Vehicle()
            .licensePlate(DEFAULT_LICENSE_PLATE)
            .quantity(DEFAULT_QUANTITY)
            .model(DEFAULT_MODEL);
        return vehicle;
    }

    @Before
    public void initTest() {
        vehicle = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicle() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();

        // Create the Vehicle
        restVehicleMockMvc.perform(post("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isCreated());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate + 1);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getLicensePlate()).isEqualTo(DEFAULT_LICENSE_PLATE);
        assertThat(testVehicle.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testVehicle.getModel()).isEqualTo(DEFAULT_MODEL);
    }

    @Test
    @Transactional
    public void createVehicleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();

        // Create the Vehicle with an existing ID
        vehicle.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVehicleMockMvc.perform(post("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isBadRequest());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllVehicles() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].licensePlate").value(hasItem(DEFAULT_LICENSE_PLATE.toString())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.intValue())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL.toString())));
    }
    

    @Test
    @Transactional
    public void getVehicle() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get the vehicle
        restVehicleMockMvc.perform(get("/api/vehicles/{id}", vehicle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vehicle.getId().intValue()))
            .andExpect(jsonPath("$.licensePlate").value(DEFAULT_LICENSE_PLATE.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.intValue()))
            .andExpect(jsonPath("$.model").value(DEFAULT_MODEL.toString()));
    }

    @Test
    @Transactional
    public void getAllVehiclesByLicensePlateIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where licensePlate equals to DEFAULT_LICENSE_PLATE
        defaultVehicleShouldBeFound("licensePlate.equals=" + DEFAULT_LICENSE_PLATE);

        // Get all the vehicleList where licensePlate equals to UPDATED_LICENSE_PLATE
        defaultVehicleShouldNotBeFound("licensePlate.equals=" + UPDATED_LICENSE_PLATE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByLicensePlateIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where licensePlate in DEFAULT_LICENSE_PLATE or UPDATED_LICENSE_PLATE
        defaultVehicleShouldBeFound("licensePlate.in=" + DEFAULT_LICENSE_PLATE + "," + UPDATED_LICENSE_PLATE);

        // Get all the vehicleList where licensePlate equals to UPDATED_LICENSE_PLATE
        defaultVehicleShouldNotBeFound("licensePlate.in=" + UPDATED_LICENSE_PLATE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByLicensePlateIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where licensePlate is not null
        defaultVehicleShouldBeFound("licensePlate.specified=true");

        // Get all the vehicleList where licensePlate is null
        defaultVehicleShouldNotBeFound("licensePlate.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where quantity equals to DEFAULT_QUANTITY
        defaultVehicleShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the vehicleList where quantity equals to UPDATED_QUANTITY
        defaultVehicleShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllVehiclesByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultVehicleShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the vehicleList where quantity equals to UPDATED_QUANTITY
        defaultVehicleShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllVehiclesByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where quantity is not null
        defaultVehicleShouldBeFound("quantity.specified=true");

        // Get all the vehicleList where quantity is null
        defaultVehicleShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model equals to DEFAULT_MODEL
        defaultVehicleShouldBeFound("model.equals=" + DEFAULT_MODEL);

        // Get all the vehicleList where model equals to UPDATED_MODEL
        defaultVehicleShouldNotBeFound("model.equals=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model in DEFAULT_MODEL or UPDATED_MODEL
        defaultVehicleShouldBeFound("model.in=" + DEFAULT_MODEL + "," + UPDATED_MODEL);

        // Get all the vehicleList where model equals to UPDATED_MODEL
        defaultVehicleShouldNotBeFound("model.in=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model is not null
        defaultVehicleShouldBeFound("model.specified=true");

        // Get all the vehicleList where model is null
        defaultVehicleShouldNotBeFound("model.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByCarrierIsEqualToSomething() throws Exception {
        // Initialize the database
        Carrier carrier = CarrierResourceIntTest.createEntity(em);
        em.persist(carrier);
        em.flush();
        vehicle.setCarrier(carrier);
        vehicleRepository.saveAndFlush(vehicle);
        Long carrierId = carrier.getId();

        // Get all the vehicleList where carrier equals to carrierId
        defaultVehicleShouldBeFound("carrierId.equals=" + carrierId);

        // Get all the vehicleList where carrier equals to carrierId + 1
        defaultVehicleShouldNotBeFound("carrierId.equals=" + (carrierId + 1));
    }


    @Test
    @Transactional
    public void getAllVehiclesByCapacityIsEqualToSomething() throws Exception {
        // Initialize the database
        Capacity capacity = CapacityResourceIntTest.createEntity(em);
        em.persist(capacity);
        em.flush();
        vehicle.setCapacity(capacity);
        vehicleRepository.saveAndFlush(vehicle);
        Long capacityId = capacity.getId();

        // Get all the vehicleList where capacity equals to capacityId
        defaultVehicleShouldBeFound("capacityId.equals=" + capacityId);

        // Get all the vehicleList where capacity equals to capacityId + 1
        defaultVehicleShouldNotBeFound("capacityId.equals=" + (capacityId + 1));
    }


    @Test
    @Transactional
    public void getAllVehiclesByBrandIsEqualToSomething() throws Exception {
        // Initialize the database
        Brand brand = BrandResourceIntTest.createEntity(em);
        em.persist(brand);
        em.flush();
        vehicle.setBrand(brand);
        vehicleRepository.saveAndFlush(vehicle);
        Long brandId = brand.getId();

        // Get all the vehicleList where brand equals to brandId
        defaultVehicleShouldBeFound("brandId.equals=" + brandId);

        // Get all the vehicleList where brand equals to brandId + 1
        defaultVehicleShouldNotBeFound("brandId.equals=" + (brandId + 1));
    }


    @Test
    @Transactional
    public void getAllVehiclesByIdvehicleIsEqualToSomething() throws Exception {
        // Initialize the database
        Roadmap idvehicle = RoadmapResourceIntTest.createEntity(em);
        em.persist(idvehicle);
        em.flush();
        vehicle.addIdvehicle(idvehicle);
        vehicleRepository.saveAndFlush(vehicle);
        Long idvehicleId = idvehicle.getId();

        // Get all the vehicleList where idvehicle equals to idvehicleId
        defaultVehicleShouldBeFound("idvehicleId.equals=" + idvehicleId);

        // Get all the vehicleList where idvehicle equals to idvehicleId + 1
        defaultVehicleShouldNotBeFound("idvehicleId.equals=" + (idvehicleId + 1));
    }


    @Test
    @Transactional
    public void getAllVehiclesByQuarryIsEqualToSomething() throws Exception {
        // Initialize the database
        Quarry quarry = QuarryResourceIntTest.createEntity(em);
        em.persist(quarry);
        em.flush();
        vehicle.setQuarry(quarry);
        vehicleRepository.saveAndFlush(vehicle);
        Long quarryId = quarry.getId();

        // Get all the vehicleList where quarry equals to quarryId
        defaultVehicleShouldBeFound("quarryId.equals=" + quarryId);

        // Get all the vehicleList where quarry equals to quarryId + 1
        defaultVehicleShouldNotBeFound("quarryId.equals=" + (quarryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVehicleShouldBeFound(String filter) throws Exception {
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].licensePlate").value(hasItem(DEFAULT_LICENSE_PLATE.toString())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.intValue())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVehicleShouldNotBeFound(String filter) throws Exception {
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingVehicle() throws Exception {
        // Get the vehicle
        restVehicleMockMvc.perform(get("/api/vehicles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicle() throws Exception {
        // Initialize the database
        vehicleService.save(vehicle);

        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Update the vehicle
        Vehicle updatedVehicle = vehicleRepository.findById(vehicle.getId()).get();
        // Disconnect from session so that the updates on updatedVehicle are not directly saved in db
        em.detach(updatedVehicle);
        updatedVehicle
            .licensePlate(UPDATED_LICENSE_PLATE)
            .quantity(UPDATED_QUANTITY)
            .model(UPDATED_MODEL);

        restVehicleMockMvc.perform(put("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVehicle)))
            .andExpect(status().isOk());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getLicensePlate()).isEqualTo(UPDATED_LICENSE_PLATE);
        assertThat(testVehicle.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testVehicle.getModel()).isEqualTo(UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void updateNonExistingVehicle() throws Exception {
        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Create the Vehicle

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVehicleMockMvc.perform(put("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isBadRequest());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVehicle() throws Exception {
        // Initialize the database
        vehicleService.save(vehicle);

        int databaseSizeBeforeDelete = vehicleRepository.findAll().size();

        // Get the vehicle
        restVehicleMockMvc.perform(delete("/api/vehicles/{id}", vehicle.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vehicle.class);
        Vehicle vehicle1 = new Vehicle();
        vehicle1.setId(1L);
        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId(vehicle1.getId());
        assertThat(vehicle1).isEqualTo(vehicle2);
        vehicle2.setId(2L);
        assertThat(vehicle1).isNotEqualTo(vehicle2);
        vehicle1.setId(null);
        assertThat(vehicle1).isNotEqualTo(vehicle2);
    }
}
