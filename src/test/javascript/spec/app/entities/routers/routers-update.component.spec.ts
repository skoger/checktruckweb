/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoutersUpdateComponent } from 'app/entities/routers/routers-update.component';
import { RoutersService } from 'app/entities/routers/routers.service';
import { Routers } from 'app/shared/model/routers.model';

describe('Component Tests', () => {
    describe('Routers Management Update Component', () => {
        let comp: RoutersUpdateComponent;
        let fixture: ComponentFixture<RoutersUpdateComponent>;
        let service: RoutersService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoutersUpdateComponent]
            })
                .overrideTemplate(RoutersUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoutersUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoutersService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Routers(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.routers = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Routers();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.routers = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
