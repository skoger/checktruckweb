/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoutersComponent } from 'app/entities/routers/routers.component';
import { RoutersService } from 'app/entities/routers/routers.service';
import { Routers } from 'app/shared/model/routers.model';

describe('Component Tests', () => {
    describe('Routers Management Component', () => {
        let comp: RoutersComponent;
        let fixture: ComponentFixture<RoutersComponent>;
        let service: RoutersService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoutersComponent],
                providers: []
            })
                .overrideTemplate(RoutersComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoutersComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoutersService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Routers(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.routers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
