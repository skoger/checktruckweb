/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoutersDetailComponent } from 'app/entities/routers/routers-detail.component';
import { Routers } from 'app/shared/model/routers.model';

describe('Component Tests', () => {
    describe('Routers Management Detail Component', () => {
        let comp: RoutersDetailComponent;
        let fixture: ComponentFixture<RoutersDetailComponent>;
        const route = ({ data: of({ routers: new Routers(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoutersDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RoutersDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RoutersDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.routers).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
