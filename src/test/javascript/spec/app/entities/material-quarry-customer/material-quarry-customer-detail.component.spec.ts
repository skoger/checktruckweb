/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { MaterialQuarryCustomerDetailComponent } from 'app/entities/material-quarry-customer/material-quarry-customer-detail.component';
import { MaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

describe('Component Tests', () => {
    describe('MaterialQuarryCustomer Management Detail Component', () => {
        let comp: MaterialQuarryCustomerDetailComponent;
        let fixture: ComponentFixture<MaterialQuarryCustomerDetailComponent>;
        const route = ({ data: of({ materialQuarryCustomer: new MaterialQuarryCustomer(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [MaterialQuarryCustomerDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(MaterialQuarryCustomerDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MaterialQuarryCustomerDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.materialQuarryCustomer).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
