/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { MaterialQuarryCustomerUpdateComponent } from 'app/entities/material-quarry-customer/material-quarry-customer-update.component';
import { MaterialQuarryCustomerService } from 'app/entities/material-quarry-customer/material-quarry-customer.service';
import { MaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

describe('Component Tests', () => {
    describe('MaterialQuarryCustomer Management Update Component', () => {
        let comp: MaterialQuarryCustomerUpdateComponent;
        let fixture: ComponentFixture<MaterialQuarryCustomerUpdateComponent>;
        let service: MaterialQuarryCustomerService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [MaterialQuarryCustomerUpdateComponent]
            })
                .overrideTemplate(MaterialQuarryCustomerUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MaterialQuarryCustomerUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MaterialQuarryCustomerService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new MaterialQuarryCustomer(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.materialQuarryCustomer = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new MaterialQuarryCustomer();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.materialQuarryCustomer = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
