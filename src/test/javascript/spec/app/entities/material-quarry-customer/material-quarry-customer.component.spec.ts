/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChecktruckwebTestModule } from '../../../test.module';
import { MaterialQuarryCustomerComponent } from 'app/entities/material-quarry-customer/material-quarry-customer.component';
import { MaterialQuarryCustomerService } from 'app/entities/material-quarry-customer/material-quarry-customer.service';
import { MaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

describe('Component Tests', () => {
    describe('MaterialQuarryCustomer Management Component', () => {
        let comp: MaterialQuarryCustomerComponent;
        let fixture: ComponentFixture<MaterialQuarryCustomerComponent>;
        let service: MaterialQuarryCustomerService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [MaterialQuarryCustomerComponent],
                providers: []
            })
                .overrideTemplate(MaterialQuarryCustomerComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MaterialQuarryCustomerComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MaterialQuarryCustomerService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new MaterialQuarryCustomer(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.materialQuarryCustomers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
