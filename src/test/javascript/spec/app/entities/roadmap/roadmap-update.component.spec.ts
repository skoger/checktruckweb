/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoadmapUpdateComponent } from 'app/entities/roadmap/roadmap-update.component';
import { RoadmapService } from 'app/entities/roadmap/roadmap.service';
import { Roadmap } from 'app/shared/model/roadmap.model';

describe('Component Tests', () => {
    describe('Roadmap Management Update Component', () => {
        let comp: RoadmapUpdateComponent;
        let fixture: ComponentFixture<RoadmapUpdateComponent>;
        let service: RoadmapService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoadmapUpdateComponent]
            })
                .overrideTemplate(RoadmapUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoadmapUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoadmapService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Roadmap(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.roadmap = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Roadmap();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.roadmap = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
