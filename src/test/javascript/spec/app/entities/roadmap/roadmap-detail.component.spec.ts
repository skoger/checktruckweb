/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoadmapDetailComponent } from 'app/entities/roadmap/roadmap-detail.component';
import { Roadmap } from 'app/shared/model/roadmap.model';

describe('Component Tests', () => {
    describe('Roadmap Management Detail Component', () => {
        let comp: RoadmapDetailComponent;
        let fixture: ComponentFixture<RoadmapDetailComponent>;
        const route = ({ data: of({ roadmap: new Roadmap(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoadmapDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RoadmapDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RoadmapDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.roadmap).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
