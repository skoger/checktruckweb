/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoadmapComponent } from 'app/entities/roadmap/roadmap.component';
import { RoadmapService } from 'app/entities/roadmap/roadmap.service';
import { Roadmap } from 'app/shared/model/roadmap.model';

describe('Component Tests', () => {
    describe('Roadmap Management Component', () => {
        let comp: RoadmapComponent;
        let fixture: ComponentFixture<RoadmapComponent>;
        let service: RoadmapService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoadmapComponent],
                providers: []
            })
                .overrideTemplate(RoadmapComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RoadmapComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoadmapService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Roadmap(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.roadmaps[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
