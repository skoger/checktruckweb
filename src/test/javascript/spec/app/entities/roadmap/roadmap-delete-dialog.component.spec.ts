/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ChecktruckwebTestModule } from '../../../test.module';
import { RoadmapDeleteDialogComponent } from 'app/entities/roadmap/roadmap-delete-dialog.component';
import { RoadmapService } from 'app/entities/roadmap/roadmap.service';

describe('Component Tests', () => {
    describe('Roadmap Management Delete Component', () => {
        let comp: RoadmapDeleteDialogComponent;
        let fixture: ComponentFixture<RoadmapDeleteDialogComponent>;
        let service: RoadmapService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [RoadmapDeleteDialogComponent]
            })
                .overrideTemplate(RoadmapDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RoadmapDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoadmapService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
