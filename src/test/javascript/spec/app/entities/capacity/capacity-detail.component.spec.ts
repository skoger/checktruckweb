/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { CapacityDetailComponent } from 'app/entities/capacity/capacity-detail.component';
import { Capacity } from 'app/shared/model/capacity.model';

describe('Component Tests', () => {
    describe('Capacity Management Detail Component', () => {
        let comp: CapacityDetailComponent;
        let fixture: ComponentFixture<CapacityDetailComponent>;
        const route = ({ data: of({ capacity: new Capacity(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [CapacityDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CapacityDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CapacityDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.capacity).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
