/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChecktruckwebTestModule } from '../../../test.module';
import { CapacityComponent } from 'app/entities/capacity/capacity.component';
import { CapacityService } from 'app/entities/capacity/capacity.service';
import { Capacity } from 'app/shared/model/capacity.model';

describe('Component Tests', () => {
    describe('Capacity Management Component', () => {
        let comp: CapacityComponent;
        let fixture: ComponentFixture<CapacityComponent>;
        let service: CapacityService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [CapacityComponent],
                providers: []
            })
                .overrideTemplate(CapacityComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CapacityComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CapacityService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Capacity(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.capacities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
