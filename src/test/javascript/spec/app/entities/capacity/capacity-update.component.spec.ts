/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { CapacityUpdateComponent } from 'app/entities/capacity/capacity-update.component';
import { CapacityService } from 'app/entities/capacity/capacity.service';
import { Capacity } from 'app/shared/model/capacity.model';

describe('Component Tests', () => {
    describe('Capacity Management Update Component', () => {
        let comp: CapacityUpdateComponent;
        let fixture: ComponentFixture<CapacityUpdateComponent>;
        let service: CapacityService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [CapacityUpdateComponent]
            })
                .overrideTemplate(CapacityUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CapacityUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CapacityService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Capacity(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.capacity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Capacity();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.capacity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
