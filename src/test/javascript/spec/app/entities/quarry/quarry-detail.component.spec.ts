/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { QuarryDetailComponent } from 'app/entities/quarry/quarry-detail.component';
import { Quarry } from 'app/shared/model/quarry.model';

describe('Component Tests', () => {
    describe('Quarry Management Detail Component', () => {
        let comp: QuarryDetailComponent;
        let fixture: ComponentFixture<QuarryDetailComponent>;
        const route = ({ data: of({ quarry: new Quarry(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [QuarryDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(QuarryDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuarryDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.quarry).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
