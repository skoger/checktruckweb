/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ChecktruckwebTestModule } from '../../../test.module';
import { QuarryDeleteDialogComponent } from 'app/entities/quarry/quarry-delete-dialog.component';
import { QuarryService } from 'app/entities/quarry/quarry.service';

describe('Component Tests', () => {
    describe('Quarry Management Delete Component', () => {
        let comp: QuarryDeleteDialogComponent;
        let fixture: ComponentFixture<QuarryDeleteDialogComponent>;
        let service: QuarryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [QuarryDeleteDialogComponent]
            })
                .overrideTemplate(QuarryDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuarryDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuarryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
