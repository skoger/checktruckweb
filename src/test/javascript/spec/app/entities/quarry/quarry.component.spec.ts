/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChecktruckwebTestModule } from '../../../test.module';
import { QuarryComponent } from 'app/entities/quarry/quarry.component';
import { QuarryService } from 'app/entities/quarry/quarry.service';
import { Quarry } from 'app/shared/model/quarry.model';

describe('Component Tests', () => {
    describe('Quarry Management Component', () => {
        let comp: QuarryComponent;
        let fixture: ComponentFixture<QuarryComponent>;
        let service: QuarryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [QuarryComponent],
                providers: []
            })
                .overrideTemplate(QuarryComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(QuarryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuarryService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Quarry(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.quarries[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
