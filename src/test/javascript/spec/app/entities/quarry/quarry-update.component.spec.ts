/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ChecktruckwebTestModule } from '../../../test.module';
import { QuarryUpdateComponent } from 'app/entities/quarry/quarry-update.component';
import { QuarryService } from 'app/entities/quarry/quarry.service';
import { Quarry } from 'app/shared/model/quarry.model';

describe('Component Tests', () => {
    describe('Quarry Management Update Component', () => {
        let comp: QuarryUpdateComponent;
        let fixture: ComponentFixture<QuarryUpdateComponent>;
        let service: QuarryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ChecktruckwebTestModule],
                declarations: [QuarryUpdateComponent]
            })
                .overrideTemplate(QuarryUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(QuarryUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuarryService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Quarry(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.quarry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Quarry();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.quarry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
