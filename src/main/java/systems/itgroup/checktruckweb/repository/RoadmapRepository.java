package systems.itgroup.checktruckweb.repository;

import systems.itgroup.checktruckweb.domain.Roadmap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Roadmap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoadmapRepository extends JpaRepository<Roadmap, Long>, JpaSpecificationExecutor<Roadmap> {

    List<Roadmap> findByUserKey(String userKey);

}
