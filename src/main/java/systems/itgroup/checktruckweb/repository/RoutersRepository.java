package systems.itgroup.checktruckweb.repository;

import systems.itgroup.checktruckweb.domain.Routers;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Routers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoutersRepository extends JpaRepository<Routers, Long>, JpaSpecificationExecutor<Routers> {

}
