package systems.itgroup.checktruckweb.repository;

import systems.itgroup.checktruckweb.domain.Capacity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Capacity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CapacityRepository extends JpaRepository<Capacity, Long>, JpaSpecificationExecutor<Capacity> {

}
