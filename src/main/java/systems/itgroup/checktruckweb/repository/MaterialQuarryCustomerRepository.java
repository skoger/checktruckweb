package systems.itgroup.checktruckweb.repository;

import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialQuarryCustomer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialQuarryCustomerRepository extends JpaRepository<MaterialQuarryCustomer, Long>, JpaSpecificationExecutor<MaterialQuarryCustomer> {

}
