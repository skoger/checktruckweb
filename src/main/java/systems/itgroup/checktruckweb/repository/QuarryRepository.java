package systems.itgroup.checktruckweb.repository;

import systems.itgroup.checktruckweb.domain.Quarry;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Quarry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuarryRepository extends JpaRepository<Quarry, Long>, JpaSpecificationExecutor<Quarry> {

}
