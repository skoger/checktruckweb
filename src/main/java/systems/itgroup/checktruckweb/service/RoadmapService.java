package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.repository.RoadmapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Roadmap.
 */
@Service
@Transactional
public class RoadmapService {

    private final Logger log = LoggerFactory.getLogger(RoadmapService.class);

    private final RoadmapRepository roadmapRepository;

    public RoadmapService(RoadmapRepository roadmapRepository) {
        this.roadmapRepository = roadmapRepository;
    }

    /**
     * Save a roadmap.
     *
     * @param roadmap the entity to save
     * @return the persisted entity
     */
    public Roadmap save(Roadmap roadmap) {
        log.debug("Request to save Roadmap : {}", roadmap);        return roadmapRepository.save(roadmap);
    }

    /**
     * Get all the roadmaps.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Roadmap> findAll() {
        log.debug("Request to get all Roadmaps");
        return roadmapRepository.findAll();
    }


    /**
     * Get one roadmap by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Roadmap> findOne(Long id) {
        log.debug("Request to get Roadmap : {}", id);
        return roadmapRepository.findById(id);
    }

    /**
     * Delete the roadmap by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Roadmap : {}", id);
        roadmapRepository.deleteById(id);
    }
}
