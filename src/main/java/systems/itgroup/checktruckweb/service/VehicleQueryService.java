package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.Vehicle;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.VehicleRepository;
import systems.itgroup.checktruckweb.service.dto.VehicleCriteria;


/**
 * Service for executing complex queries for Vehicle entities in the database.
 * The main input is a {@link VehicleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Vehicle} or a {@link Page} of {@link Vehicle} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VehicleQueryService extends QueryService<Vehicle> {

    private final Logger log = LoggerFactory.getLogger(VehicleQueryService.class);

    private final VehicleRepository vehicleRepository;

    public VehicleQueryService(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    /**
     * Return a {@link List} of {@link Vehicle} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Vehicle> findByCriteria(VehicleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Vehicle> specification = createSpecification(criteria);
        return vehicleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Vehicle} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Vehicle> findByCriteria(VehicleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Vehicle> specification = createSpecification(criteria);
        return vehicleRepository.findAll(specification, page);
    }

    /**
     * Function to convert VehicleCriteria to a {@link Specification}
     */
    private Specification<Vehicle> createSpecification(VehicleCriteria criteria) {
        Specification<Vehicle> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Vehicle_.id));
            }
            if (criteria.getLicensePlate() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLicensePlate(), Vehicle_.licensePlate));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), Vehicle_.quantity));
            }
            if (criteria.getModel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModel(), Vehicle_.model));
            }
            if (criteria.getCarrierId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCarrierId(), Vehicle_.carrier, Carrier_.id));
            }
            if (criteria.getCapacityId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCapacityId(), Vehicle_.capacity, Capacity_.id));
            }
            if (criteria.getBrandId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getBrandId(), Vehicle_.brand, Brand_.id));
            }
            if (criteria.getIdvehicleId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getIdvehicleId(), Vehicle_.idvehicles, Roadmap_.id));
            }
            if (criteria.getQuarryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getQuarryId(), Vehicle_.quarry, Quarry_.id));
            }
        }
        return specification;
    }

}
