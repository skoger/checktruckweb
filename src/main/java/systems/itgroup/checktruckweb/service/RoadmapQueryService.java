package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.RoadmapRepository;
import systems.itgroup.checktruckweb.service.dto.RoadmapCriteria;


/**
 * Service for executing complex queries for Roadmap entities in the database.
 * The main input is a {@link RoadmapCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Roadmap} or a {@link Page} of {@link Roadmap} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RoadmapQueryService extends QueryService<Roadmap> {

    private final Logger log = LoggerFactory.getLogger(RoadmapQueryService.class);

    private final RoadmapRepository roadmapRepository;

    public RoadmapQueryService(RoadmapRepository roadmapRepository) {
        this.roadmapRepository = roadmapRepository;
    }

    /**
     * Return a {@link List} of {@link Roadmap} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Roadmap> findByCriteria(RoadmapCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Roadmap> specification = createSpecification(criteria);
        return roadmapRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Roadmap} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Roadmap> findByCriteria(RoadmapCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Roadmap> specification = createSpecification(criteria);
        return roadmapRepository.findAll(specification, page);
    }

    /**
     * Function to convert RoadmapCriteria to a {@link Specification}
     */
    private Specification<Roadmap> createSpecification(RoadmapCriteria criteria) {
        Specification<Roadmap> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Roadmap_.id));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), Roadmap_.price));
            }
            if (criteria.getSynch() != null) {
                specification = specification.and(buildSpecification(criteria.getSynch(), Roadmap_.synch));
            }
            if (criteria.getVerified() != null) {
                specification = specification.and(buildSpecification(criteria.getVerified(), Roadmap_.verified));
            }
            if (criteria.getIdUser() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIdUser(), Roadmap_.idUser));
            }
            if (criteria.getCreated() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreated(), Roadmap_.created));
            }
            if (criteria.getVerifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVerifiedDate(), Roadmap_.verifiedDate));
            }
            if (criteria.getUserKey() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserKey(), Roadmap_.userKey));
            }
            if (criteria.getUserName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserName(), Roadmap_.userName));
            }
            if (criteria.getAnnulled() != null) {
                specification = specification.and(buildSpecification(criteria.getAnnulled(), Roadmap_.annulled));
            }
            if (criteria.getRoutersId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getRoutersId(), Roadmap_.routers, Routers_.id));
            }
            if (criteria.getVehicleId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getVehicleId(), Roadmap_.vehicle, Vehicle_.id));
            }
            if (criteria.getMaterialQuarryCustomerId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMaterialQuarryCustomerId(), Roadmap_.materialQuarryCustomer, MaterialQuarryCustomer_.id));
            }
        }
        return specification;
    }

}
