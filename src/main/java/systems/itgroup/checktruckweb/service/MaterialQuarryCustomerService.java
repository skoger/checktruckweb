package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.repository.MaterialQuarryCustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing MaterialQuarryCustomer.
 */
@Service
@Transactional
public class MaterialQuarryCustomerService {

    private final Logger log = LoggerFactory.getLogger(MaterialQuarryCustomerService.class);

    private final MaterialQuarryCustomerRepository materialQuarryCustomerRepository;

    public MaterialQuarryCustomerService(MaterialQuarryCustomerRepository materialQuarryCustomerRepository) {
        this.materialQuarryCustomerRepository = materialQuarryCustomerRepository;
    }

    /**
     * Save a materialQuarryCustomer.
     *
     * @param materialQuarryCustomer the entity to save
     * @return the persisted entity
     */
    public MaterialQuarryCustomer save(MaterialQuarryCustomer materialQuarryCustomer) {
        log.debug("Request to save MaterialQuarryCustomer : {}", materialQuarryCustomer);        return materialQuarryCustomerRepository.save(materialQuarryCustomer);
    }

    /**
     * Get all the materialQuarryCustomers.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<MaterialQuarryCustomer> findAll() {
        log.debug("Request to get all MaterialQuarryCustomers");
        return materialQuarryCustomerRepository.findAll();
    }


    /**
     * Get one materialQuarryCustomer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<MaterialQuarryCustomer> findOne(Long id) {
        log.debug("Request to get MaterialQuarryCustomer : {}", id);
        return materialQuarryCustomerRepository.findById(id);
    }

    /**
     * Delete the materialQuarryCustomer by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MaterialQuarryCustomer : {}", id);
        materialQuarryCustomerRepository.deleteById(id);
    }
}
