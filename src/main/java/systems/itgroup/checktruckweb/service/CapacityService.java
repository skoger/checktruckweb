package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.Capacity;
import systems.itgroup.checktruckweb.repository.CapacityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Capacity.
 */
@Service
@Transactional
public class CapacityService {

    private final Logger log = LoggerFactory.getLogger(CapacityService.class);

    private final CapacityRepository capacityRepository;

    public CapacityService(CapacityRepository capacityRepository) {
        this.capacityRepository = capacityRepository;
    }

    /**
     * Save a capacity.
     *
     * @param capacity the entity to save
     * @return the persisted entity
     */
    public Capacity save(Capacity capacity) {
        log.debug("Request to save Capacity : {}", capacity);        return capacityRepository.save(capacity);
    }

    /**
     * Get all the capacities.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Capacity> findAll() {
        log.debug("Request to get all Capacities");
        return capacityRepository.findAll();
    }


    /**
     * Get one capacity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Capacity> findOne(Long id) {
        log.debug("Request to get Capacity : {}", id);
        return capacityRepository.findById(id);
    }

    /**
     * Delete the capacity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Capacity : {}", id);
        capacityRepository.deleteById(id);
    }
}
