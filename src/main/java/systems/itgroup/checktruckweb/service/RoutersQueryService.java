package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.Routers;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.RoutersRepository;
import systems.itgroup.checktruckweb.service.dto.RoutersCriteria;


/**
 * Service for executing complex queries for Routers entities in the database.
 * The main input is a {@link RoutersCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Routers} or a {@link Page} of {@link Routers} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RoutersQueryService extends QueryService<Routers> {

    private final Logger log = LoggerFactory.getLogger(RoutersQueryService.class);

    private final RoutersRepository routersRepository;

    public RoutersQueryService(RoutersRepository routersRepository) {
        this.routersRepository = routersRepository;
    }

    /**
     * Return a {@link List} of {@link Routers} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Routers> findByCriteria(RoutersCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Routers> specification = createSpecification(criteria);
        return routersRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Routers} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Routers> findByCriteria(RoutersCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Routers> specification = createSpecification(criteria);
        return routersRepository.findAll(specification, page);
    }

    /**
     * Function to convert RoutersCriteria to a {@link Specification}
     */
    private Specification<Routers> createSpecification(RoutersCriteria criteria) {
        Specification<Routers> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Routers_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Routers_.description));
            }
            if (criteria.getDistanceKm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDistanceKm(), Routers_.distanceKm));
            }
            if (criteria.getCost() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCost(), Routers_.cost));
            }
            if (criteria.getReportCopies() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReportCopies(), Routers_.reportCopies));
            }
            if (criteria.getRoutersRaodmapId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getRoutersRaodmapId(), Routers_.routersRaodmaps, Roadmap_.id));
            }
        }
        return specification;
    }

}
