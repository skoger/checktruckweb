package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.Routers;
import systems.itgroup.checktruckweb.repository.RoutersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Routers.
 */
@Service
@Transactional
public class RoutersService {

    private final Logger log = LoggerFactory.getLogger(RoutersService.class);

    private final RoutersRepository routersRepository;

    public RoutersService(RoutersRepository routersRepository) {
        this.routersRepository = routersRepository;
    }

    /**
     * Save a routers.
     *
     * @param routers the entity to save
     * @return the persisted entity
     */
    public Routers save(Routers routers) {
        log.debug("Request to save Routers : {}", routers);        return routersRepository.save(routers);
    }

    /**
     * Get all the routers.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Routers> findAll() {
        log.debug("Request to get all Routers");
        return routersRepository.findAll();
    }


    /**
     * Get one routers by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Routers> findOne(Long id) {
        log.debug("Request to get Routers : {}", id);
        return routersRepository.findById(id);
    }

    /**
     * Delete the routers by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Routers : {}", id);
        routersRepository.deleteById(id);
    }
}
