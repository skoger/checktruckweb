package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.Quarry;
import systems.itgroup.checktruckweb.repository.QuarryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Quarry.
 */
@Service
@Transactional
public class QuarryService {

    private final Logger log = LoggerFactory.getLogger(QuarryService.class);

    private final QuarryRepository quarryRepository;

    public QuarryService(QuarryRepository quarryRepository) {
        this.quarryRepository = quarryRepository;
    }

    /**
     * Save a quarry.
     *
     * @param quarry the entity to save
     * @return the persisted entity
     */
    public Quarry save(Quarry quarry) {
        log.debug("Request to save Quarry : {}", quarry);        return quarryRepository.save(quarry);
    }

    /**
     * Get all the quarries.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Quarry> findAll() {
        log.debug("Request to get all Quarries");
        return quarryRepository.findAll();
    }


    /**
     * Get one quarry by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Quarry> findOne(Long id) {
        log.debug("Request to get Quarry : {}", id);
        return quarryRepository.findById(id);
    }

    /**
     * Delete the quarry by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Quarry : {}", id);
        quarryRepository.deleteById(id);
    }
}
