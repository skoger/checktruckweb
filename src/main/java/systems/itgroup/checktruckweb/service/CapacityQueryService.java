package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.Capacity;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.CapacityRepository;
import systems.itgroup.checktruckweb.service.dto.CapacityCriteria;


/**
 * Service for executing complex queries for Capacity entities in the database.
 * The main input is a {@link CapacityCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Capacity} or a {@link Page} of {@link Capacity} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CapacityQueryService extends QueryService<Capacity> {

    private final Logger log = LoggerFactory.getLogger(CapacityQueryService.class);

    private final CapacityRepository capacityRepository;

    public CapacityQueryService(CapacityRepository capacityRepository) {
        this.capacityRepository = capacityRepository;
    }

    /**
     * Return a {@link List} of {@link Capacity} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Capacity> findByCriteria(CapacityCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Capacity> specification = createSpecification(criteria);
        return capacityRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Capacity} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Capacity> findByCriteria(CapacityCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Capacity> specification = createSpecification(criteria);
        return capacityRepository.findAll(specification, page);
    }

    /**
     * Function to convert CapacityCriteria to a {@link Specification}
     */
    private Specification<Capacity> createSpecification(CapacityCriteria criteria) {
        Specification<Capacity> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Capacity_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Capacity_.description));
            }
            if (criteria.getIdcapacityId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getIdcapacityId(), Capacity_.idcapacities, Vehicle_.id));
            }
        }
        return specification;
    }

}
