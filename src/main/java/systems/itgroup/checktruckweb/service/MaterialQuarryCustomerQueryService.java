package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.MaterialQuarryCustomerRepository;
import systems.itgroup.checktruckweb.service.dto.MaterialQuarryCustomerCriteria;


/**
 * Service for executing complex queries for MaterialQuarryCustomer entities in the database.
 * The main input is a {@link MaterialQuarryCustomerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MaterialQuarryCustomer} or a {@link Page} of {@link MaterialQuarryCustomer} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MaterialQuarryCustomerQueryService extends QueryService<MaterialQuarryCustomer> {

    private final Logger log = LoggerFactory.getLogger(MaterialQuarryCustomerQueryService.class);

    private final MaterialQuarryCustomerRepository materialQuarryCustomerRepository;

    public MaterialQuarryCustomerQueryService(MaterialQuarryCustomerRepository materialQuarryCustomerRepository) {
        this.materialQuarryCustomerRepository = materialQuarryCustomerRepository;
    }

    /**
     * Return a {@link List} of {@link MaterialQuarryCustomer} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MaterialQuarryCustomer> findByCriteria(MaterialQuarryCustomerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MaterialQuarryCustomer> specification = createSpecification(criteria);
        return materialQuarryCustomerRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MaterialQuarryCustomer} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MaterialQuarryCustomer> findByCriteria(MaterialQuarryCustomerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MaterialQuarryCustomer> specification = createSpecification(criteria);
        return materialQuarryCustomerRepository.findAll(specification, page);
    }

    /**
     * Function to convert MaterialQuarryCustomerCriteria to a {@link Specification}
     */
    private Specification<MaterialQuarryCustomer> createSpecification(MaterialQuarryCustomerCriteria criteria) {
        Specification<MaterialQuarryCustomer> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MaterialQuarryCustomer_.id));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), MaterialQuarryCustomer_.price));
            }
            if (criteria.getCustomerId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCustomerId(), MaterialQuarryCustomer_.customer, Customer_.id));
            }
            if (criteria.getQuarryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getQuarryId(), MaterialQuarryCustomer_.quarry, Quarry_.id));
            }
            if (criteria.getMaterialId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMaterialId(), MaterialQuarryCustomer_.material, Material_.id));
            }
            if (criteria.getIdmaterialquarryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getIdmaterialquarryId(), MaterialQuarryCustomer_.idmaterialquarries, Roadmap_.id));
            }
        }
        return specification;
    }

}
