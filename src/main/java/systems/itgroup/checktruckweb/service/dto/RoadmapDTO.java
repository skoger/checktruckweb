package systems.itgroup.checktruckweb.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;


/**
 * xxxxxxxxxx
 */
public class RoadmapDTO implements Serializable {
    private static final long serialVersionUID = 1L;


    private BigDecimal price;

//    private Boolean synch;

    private Boolean verified;

    private String idUser;

    private ZonedDateTime created;

    private ZonedDateTime verifiedDate;

    private String userKey;

    private String userName;

    private Boolean annulled;

    private Long routersId;

    private Long vehicleId;

    private Long materialQuarryCustomerId;

    public RoadmapDTO() {
    }

    public BigDecimal getPrice() { return price; }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

//    public Boolean getSynch() {
//        return synch;
//    }
//
//    public void setSynch(Boolean synch) {
//        this.synch = synch;
//    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(ZonedDateTime verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getAnnulled() {
        return annulled;
    }

    public void setAnnulled(Boolean annulled) {
        this.annulled = annulled;
    }

    public Long getRoutersId() {
        return routersId;
    }

    public void setRoutersId(Long routersId) {
        this.routersId = routersId;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getMaterialQuarryCustomerId() {
        return materialQuarryCustomerId;
    }

    public void setMaterialQuarryCustomerId(Long materialQuarryCustomerId) {
        this.materialQuarryCustomerId = materialQuarryCustomerId;
    }

    @Override
    public String toString() {
        return "RoadmapDTO{" +
            "price=" + price +
//            ", synch=" + synch +
            ", verified=" + verified +
            ", idUser='" + idUser + '\'' +
            ", created=" + created +
            ", verifiedDate=" + verifiedDate +
            ", userKey='" + userKey + '\'' +
            ", userName='" + userName + '\'' +
            ", annulled=" + annulled +
            ", routersId=" + routersId +
            ", vehicleId=" + vehicleId +
            ", materialQuarryCustomerId=" + materialQuarryCustomerId +
            '}';
    }
}
