package systems.itgroup.checktruckweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;





/**
 * Criteria class for the Vehicle entity. This class is used in VehicleResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /vehicles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VehicleCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter licensePlate;

    private BigDecimalFilter quantity;

    private StringFilter model;

    private LongFilter carrierId;

    private LongFilter capacityId;

    private LongFilter brandId;

    private LongFilter idvehicleId;

    private LongFilter quarryId;

    public VehicleCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(StringFilter licensePlate) {
        this.licensePlate = licensePlate;
    }

    public BigDecimalFilter getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimalFilter quantity) {
        this.quantity = quantity;
    }

    public StringFilter getModel() {
        return model;
    }

    public void setModel(StringFilter model) {
        this.model = model;
    }

    public LongFilter getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(LongFilter carrierId) {
        this.carrierId = carrierId;
    }

    public LongFilter getCapacityId() {
        return capacityId;
    }

    public void setCapacityId(LongFilter capacityId) {
        this.capacityId = capacityId;
    }

    public LongFilter getBrandId() {
        return brandId;
    }

    public void setBrandId(LongFilter brandId) {
        this.brandId = brandId;
    }

    public LongFilter getIdvehicleId() {
        return idvehicleId;
    }

    public void setIdvehicleId(LongFilter idvehicleId) {
        this.idvehicleId = idvehicleId;
    }

    public LongFilter getQuarryId() {
        return quarryId;
    }

    public void setQuarryId(LongFilter quarryId) {
        this.quarryId = quarryId;
    }

    @Override
    public String toString() {
        return "VehicleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (licensePlate != null ? "licensePlate=" + licensePlate + ", " : "") +
                (quantity != null ? "quantity=" + quantity + ", " : "") +
                (model != null ? "model=" + model + ", " : "") +
                (carrierId != null ? "carrierId=" + carrierId + ", " : "") +
                (capacityId != null ? "capacityId=" + capacityId + ", " : "") +
                (brandId != null ? "brandId=" + brandId + ", " : "") +
                (idvehicleId != null ? "idvehicleId=" + idvehicleId + ", " : "") +
                (quarryId != null ? "quarryId=" + quarryId + ", " : "") +
            "}";
    }

}
