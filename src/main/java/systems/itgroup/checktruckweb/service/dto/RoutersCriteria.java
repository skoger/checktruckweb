package systems.itgroup.checktruckweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;





/**
 * Criteria class for the Routers entity. This class is used in RoutersResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /routers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RoutersCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter description;

    private BigDecimalFilter distanceKm;

    private BigDecimalFilter cost;

    private IntegerFilter reportCopies;

    private LongFilter routersRaodmapId;

    public RoutersCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public BigDecimalFilter getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(BigDecimalFilter distanceKm) {
        this.distanceKm = distanceKm;
    }

    public BigDecimalFilter getCost() {
        return cost;
    }

    public void setCost(BigDecimalFilter cost) {
        this.cost = cost;
    }

    public IntegerFilter getReportCopies() {
        return reportCopies;
    }

    public void setReportCopies(IntegerFilter reportCopies) {
        this.reportCopies = reportCopies;
    }

    public LongFilter getRoutersRaodmapId() {
        return routersRaodmapId;
    }

    public void setRoutersRaodmapId(LongFilter routersRaodmapId) {
        this.routersRaodmapId = routersRaodmapId;
    }

    @Override
    public String toString() {
        return "RoutersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (distanceKm != null ? "distanceKm=" + distanceKm + ", " : "") +
                (cost != null ? "cost=" + cost + ", " : "") +
                (reportCopies != null ? "reportCopies=" + reportCopies + ", " : "") +
                (routersRaodmapId != null ? "routersRaodmapId=" + routersRaodmapId + ", " : "") +
            "}";
    }

}
