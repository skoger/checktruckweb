package systems.itgroup.checktruckweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;





/**
 * Criteria class for the MaterialQuarryCustomer entity. This class is used in MaterialQuarryCustomerResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /material-quarry-customers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MaterialQuarryCustomerCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private BigDecimalFilter price;

    private LongFilter customerId;

    private LongFilter quarryId;

    private LongFilter materialId;

    private LongFilter idmaterialquarryId;

    public MaterialQuarryCustomerCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getPrice() {
        return price;
    }

    public void setPrice(BigDecimalFilter price) {
        this.price = price;
    }

    public LongFilter getCustomerId() {
        return customerId;
    }

    public void setCustomerId(LongFilter customerId) {
        this.customerId = customerId;
    }

    public LongFilter getQuarryId() {
        return quarryId;
    }

    public void setQuarryId(LongFilter quarryId) {
        this.quarryId = quarryId;
    }

    public LongFilter getMaterialId() {
        return materialId;
    }

    public void setMaterialId(LongFilter materialId) {
        this.materialId = materialId;
    }

    public LongFilter getIdmaterialquarryId() {
        return idmaterialquarryId;
    }

    public void setIdmaterialquarryId(LongFilter idmaterialquarryId) {
        this.idmaterialquarryId = idmaterialquarryId;
    }

    @Override
    public String toString() {
        return "MaterialQuarryCustomerCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (customerId != null ? "customerId=" + customerId + ", " : "") +
                (quarryId != null ? "quarryId=" + quarryId + ", " : "") +
                (materialId != null ? "materialId=" + materialId + ", " : "") +
                (idmaterialquarryId != null ? "idmaterialquarryId=" + idmaterialquarryId + ", " : "") +
            "}";
    }

}
