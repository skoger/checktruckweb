package systems.itgroup.checktruckweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;


import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Roadmap entity. This class is used in RoadmapResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /roadmaps?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RoadmapCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private BigDecimalFilter price;

    private BooleanFilter synch;

    private BooleanFilter verified;

    private StringFilter idUser;

    private ZonedDateTimeFilter created;

    private ZonedDateTimeFilter verifiedDate;

    private StringFilter userKey;

    private StringFilter userName;

    private BooleanFilter annulled;

    private LongFilter routersId;

    private LongFilter vehicleId;

    private LongFilter materialQuarryCustomerId;

    public RoadmapCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getPrice() {
        return price;
    }

    public void setPrice(BigDecimalFilter price) {
        this.price = price;
    }

    public BooleanFilter getSynch() {
        return synch;
    }

    public void setSynch(BooleanFilter synch) {
        this.synch = synch;
    }

    public BooleanFilter getVerified() {
        return verified;
    }

    public void setVerified(BooleanFilter verified) {
        this.verified = verified;
    }

    public StringFilter getIdUser() {
        return idUser;
    }

    public void setIdUser(StringFilter idUser) {
        this.idUser = idUser;
    }

    public ZonedDateTimeFilter getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTimeFilter created) {
        this.created = created;
    }

    public ZonedDateTimeFilter getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(ZonedDateTimeFilter verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public StringFilter getUserKey() {
        return userKey;
    }

    public void setUserKey(StringFilter userKey) {
        this.userKey = userKey;
    }

    public StringFilter getUserName() {
        return userName;
    }

    public void setUserName(StringFilter userName) {
        this.userName = userName;
    }

    public BooleanFilter getAnnulled() {
        return annulled;
    }

    public void setAnnulled(BooleanFilter annulled) {
        this.annulled = annulled;
    }

    public LongFilter getRoutersId() {
        return routersId;
    }

    public void setRoutersId(LongFilter routersId) {
        this.routersId = routersId;
    }

    public LongFilter getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(LongFilter vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LongFilter getMaterialQuarryCustomerId() {
        return materialQuarryCustomerId;
    }

    public void setMaterialQuarryCustomerId(LongFilter materialQuarryCustomerId) {
        this.materialQuarryCustomerId = materialQuarryCustomerId;
    }

    @Override
    public String toString() {
        return "RoadmapCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (synch != null ? "synch=" + synch + ", " : "") +
                (verified != null ? "verified=" + verified + ", " : "") +
                (idUser != null ? "idUser=" + idUser + ", " : "") +
                (created != null ? "created=" + created + ", " : "") +
                (verifiedDate != null ? "verifiedDate=" + verifiedDate + ", " : "") +
                (userKey != null ? "userKey=" + userKey + ", " : "") +
                (userName != null ? "userName=" + userName + ", " : "") +
                (annulled != null ? "annulled=" + annulled + ", " : "") +
                (routersId != null ? "routersId=" + routersId + ", " : "") +
                (vehicleId != null ? "vehicleId=" + vehicleId + ", " : "") +
                (materialQuarryCustomerId != null ? "materialQuarryCustomerId=" + materialQuarryCustomerId + ", " : "") +
            "}";
    }

}
