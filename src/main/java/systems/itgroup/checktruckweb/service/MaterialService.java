package systems.itgroup.checktruckweb.service;

import systems.itgroup.checktruckweb.domain.Material;
import systems.itgroup.checktruckweb.repository.MaterialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Material.
 */
@Service
@Transactional
public class MaterialService {

    private final Logger log = LoggerFactory.getLogger(MaterialService.class);

    private final MaterialRepository materialRepository;

    public MaterialService(MaterialRepository materialRepository) {
        this.materialRepository = materialRepository;
    }

    /**
     * Save a material.
     *
     * @param material the entity to save
     * @return the persisted entity
     */
    public Material save(Material material) {
        log.debug("Request to save Material : {}", material);        return materialRepository.save(material);
    }

    /**
     * Get all the materials.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Material> findAll() {
        log.debug("Request to get all Materials");
        return materialRepository.findAll();
    }


    /**
     * Get one material by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Material> findOne(Long id) {
        log.debug("Request to get Material : {}", id);
        return materialRepository.findById(id);
    }

    /**
     * Delete the material by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Material : {}", id);
        materialRepository.deleteById(id);
    }
}
