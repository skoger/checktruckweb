package systems.itgroup.checktruckweb.service;

import org.hibernate.tool.schema.internal.ExceptionHandlerLoggedImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import systems.itgroup.checktruckweb.domain.Carrier;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.repository.*;
import systems.itgroup.checktruckweb.service.dto.RoadmapDTO;

import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Carrier.
 */
@Service
@Transactional
public class ITCheckTruckService {

    private final Logger log = LoggerFactory.getLogger(ITCheckTruckService.class);

    @Autowired
    private RoutersRepository routersRepository;
    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private MaterialQuarryCustomerRepository materialQuarryCustomerRepository;
    @Autowired
    private RoadmapRepository roadmapRepository;

    /**
     * Save a Roadmap.
     *
     * @param roadmapDto the entity to save
     * @return the persisted entity
     */
    public Roadmap verification(RoadmapDTO roadmapDto) throws Exception {
        log.debug("Request to save RoadMap : {}", roadmapDto);
        ZoneId zona=ZoneId.of("America/La_Paz");

        List<Roadmap> roadmaps= roadmapRepository.findByUserKey(roadmapDto.getUserKey());
        if (roadmaps.size()>0){
            // ya hay
            Roadmap roadmap =roadmaps.get(0);
            roadmap.setAnnulled(roadmapDto.getAnnulled());
            roadmap.setVerified(roadmapDto.getVerified());
            roadmap.setVerifiedDate(roadmapDto.getVerifiedDate().withZoneSameLocal(zona));
            return roadmapRepository.save(roadmap);
        }else {
            // Np hay
            Roadmap roadmap = new Roadmap();
            roadmap.setAnnulled(roadmapDto.getAnnulled());
            roadmap.setCreated(roadmapDto.getCreated().withZoneSameLocal(zona));
            roadmap.setIdUser(roadmapDto.getIdUser());
            roadmap.setMaterialQuarryCustomer(materialQuarryCustomerRepository.getOne(roadmapDto.getMaterialQuarryCustomerId()));
            roadmap.setPrice(roadmapDto.getPrice());
            roadmap.setRouters(routersRepository.getOne(roadmapDto.getRoutersId()));
            roadmap.setSynch(true);
            roadmap.setUserKey(roadmapDto.getUserKey());
            roadmap.setUserName(roadmapDto.getUserName());
            roadmap.setVehicle(vehicleRepository.getOne(roadmapDto.getVehicleId()));
            roadmap.setVerified(roadmapDto.getVerified());
            roadmap.setVerifiedDate(roadmapDto.getVerifiedDate().withZoneSameLocal(zona));
            return roadmapRepository.save(roadmap);
        }
    }

//    /**
//     * Get all the carriers.
//     *
//     * @return the list of entities
//     */
//    @Transactional(readOnly = true)
//    public List<Carrier> findAll() {
//        log.debug("Request to get all Carriers");
//        return carrierRepository.findAll();
//    }
//
//
//    /**
//     * Get one carrier by id.
//     *
//     * @param id the id of the entity
//     * @return the entity
//     */
//    @Transactional(readOnly = true)
//    public Optional<Carrier> findOne(Long id) {
//        log.debug("Request to get Carrier : {}", id);
//        return carrierRepository.findById(id);
//    }
//
//    /**
//     * Delete the carrier by id.
//     *
//     * @param id the id of the entity
//     */
//    public void delete(Long id) {
//        log.debug("Request to delete Carrier : {}", id);
//        carrierRepository.deleteById(id);
//    }
}
