package systems.itgroup.checktruckweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import systems.itgroup.checktruckweb.domain.Carrier;
import systems.itgroup.checktruckweb.domain.*; // for static metamodels
import systems.itgroup.checktruckweb.repository.CarrierRepository;
import systems.itgroup.checktruckweb.service.dto.CarrierCriteria;


/**
 * Service for executing complex queries for Carrier entities in the database.
 * The main input is a {@link CarrierCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Carrier} or a {@link Page} of {@link Carrier} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CarrierQueryService extends QueryService<Carrier> {

    private final Logger log = LoggerFactory.getLogger(CarrierQueryService.class);

    private final CarrierRepository carrierRepository;

    public CarrierQueryService(CarrierRepository carrierRepository) {
        this.carrierRepository = carrierRepository;
    }

    /**
     * Return a {@link List} of {@link Carrier} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Carrier> findByCriteria(CarrierCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Carrier> specification = createSpecification(criteria);
        return carrierRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Carrier} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Carrier> findByCriteria(CarrierCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Carrier> specification = createSpecification(criteria);
        return carrierRepository.findAll(specification, page);
    }

    /**
     * Function to convert CarrierCriteria to a {@link Specification}
     */
    private Specification<Carrier> createSpecification(CarrierCriteria criteria) {
        Specification<Carrier> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Carrier_.id));
            }
            if (criteria.getBusinessName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBusinessName(), Carrier_.businessName));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Carrier_.address));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), Carrier_.phone));
            }
            if (criteria.getContact() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContact(), Carrier_.contact));
            }
            if (criteria.getContactPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactPhone(), Carrier_.contactPhone));
            }
            if (criteria.getIdcarrierId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getIdcarrierId(), Carrier_.idcarriers, Vehicle_.id));
            }
        }
        return specification;
    }

}
