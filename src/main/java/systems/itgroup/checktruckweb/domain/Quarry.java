package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Quarry.
 */
@Entity
@Table(name = "quarry")
public class Quarry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(mappedBy = "quarry")
    private Set<MaterialQuarryCustomer> idquarries = new HashSet<>();

    @OneToMany(mappedBy = "quarry")
    private Set<Vehicle> vehicles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Quarry description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MaterialQuarryCustomer> getIdquarries() {
        return idquarries;
    }

    public Quarry idquarries(Set<MaterialQuarryCustomer> materialQuarryCustomers) {
        this.idquarries = materialQuarryCustomers;
        return this;
    }

    public Quarry addIdquarry(MaterialQuarryCustomer materialQuarryCustomer) {
        this.idquarries.add(materialQuarryCustomer);
        materialQuarryCustomer.setQuarry(this);
        return this;
    }

    public Quarry removeIdquarry(MaterialQuarryCustomer materialQuarryCustomer) {
        this.idquarries.remove(materialQuarryCustomer);
        materialQuarryCustomer.setQuarry(null);
        return this;
    }

    public void setIdquarries(Set<MaterialQuarryCustomer> materialQuarryCustomers) {
        this.idquarries = materialQuarryCustomers;
    }

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }

    public Quarry vehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
        return this;
    }

    public Quarry addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
        vehicle.setQuarry(this);
        return this;
    }

    public Quarry removeVehicle(Vehicle vehicle) {
        this.vehicles.remove(vehicle);
        vehicle.setQuarry(null);
        return this;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Quarry quarry = (Quarry) o;
        if (quarry.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), quarry.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Quarry{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
