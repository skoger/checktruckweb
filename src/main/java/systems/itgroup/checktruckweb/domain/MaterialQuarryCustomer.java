package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialQuarryCustomer.
 */
@Entity
@Table(name = "material_quarry_customer")
public class MaterialQuarryCustomer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "price", precision = 10, scale = 2, nullable = false)
    private BigDecimal price;

    @ManyToOne
    @JsonIgnoreProperties("idcustomers")
    private Customer customer;

    @ManyToOne
    @JsonIgnoreProperties("idquarries")
    private Quarry quarry;

    @ManyToOne
    @JsonIgnoreProperties("idmaterials")
    private Material material;

    /**
     * A relationship
     */
    @ApiModelProperty(value = "A relationship")
    @OneToMany(mappedBy = "materialQuarryCustomer")
    private Set<Roadmap> idmaterialquarries = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public MaterialQuarryCustomer price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Customer getCustomer() {
        return customer;
    }

    public MaterialQuarryCustomer customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Quarry getQuarry() {
        return quarry;
    }

    public MaterialQuarryCustomer quarry(Quarry quarry) {
        this.quarry = quarry;
        return this;
    }

    public void setQuarry(Quarry quarry) {
        this.quarry = quarry;
    }

    public Material getMaterial() {
        return material;
    }

    public MaterialQuarryCustomer material(Material material) {
        this.material = material;
        return this;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Set<Roadmap> getIdmaterialquarries() {
        return idmaterialquarries;
    }

    public MaterialQuarryCustomer idmaterialquarries(Set<Roadmap> roadmaps) {
        this.idmaterialquarries = roadmaps;
        return this;
    }

    public MaterialQuarryCustomer addIdmaterialquarry(Roadmap roadmap) {
        this.idmaterialquarries.add(roadmap);
        roadmap.setMaterialQuarryCustomer(this);
        return this;
    }

    public MaterialQuarryCustomer removeIdmaterialquarry(Roadmap roadmap) {
        this.idmaterialquarries.remove(roadmap);
        roadmap.setMaterialQuarryCustomer(null);
        return this;
    }

    public void setIdmaterialquarries(Set<Roadmap> roadmaps) {
        this.idmaterialquarries = roadmaps;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MaterialQuarryCustomer materialQuarryCustomer = (MaterialQuarryCustomer) o;
        if (materialQuarryCustomer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), materialQuarryCustomer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MaterialQuarryCustomer{" +
            "id=" + getId() +
            ", price=" + getPrice() +
            "}";
    }
}
