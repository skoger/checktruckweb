package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Capacity.
 */
@Entity
@Table(name = "capacity")
public class Capacity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(mappedBy = "capacity")
    private Set<Vehicle> idcapacities = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Capacity description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Vehicle> getIdcapacities() {
        return idcapacities;
    }

    public Capacity idcapacities(Set<Vehicle> vehicles) {
        this.idcapacities = vehicles;
        return this;
    }

    public Capacity addIdcapacity(Vehicle vehicle) {
        this.idcapacities.add(vehicle);
        vehicle.setCapacity(this);
        return this;
    }

    public Capacity removeIdcapacity(Vehicle vehicle) {
        this.idcapacities.remove(vehicle);
        vehicle.setCapacity(null);
        return this;
    }

    public void setIdcapacities(Set<Vehicle> vehicles) {
        this.idcapacities = vehicles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Capacity capacity = (Capacity) o;
        if (capacity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), capacity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Capacity{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
