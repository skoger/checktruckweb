package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Material.
 */
@Entity
@Table(name = "material")
public class Material implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    /**
     * A relationship
     */
    @ApiModelProperty(value = "A relationship")
    @OneToMany(mappedBy = "material")
    private Set<MaterialQuarryCustomer> idmaterials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Material description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MaterialQuarryCustomer> getIdmaterials() {
        return idmaterials;
    }

    public Material idmaterials(Set<MaterialQuarryCustomer> materialQuarryCustomers) {
        this.idmaterials = materialQuarryCustomers;
        return this;
    }

    public Material addIdmaterial(MaterialQuarryCustomer materialQuarryCustomer) {
        this.idmaterials.add(materialQuarryCustomer);
        materialQuarryCustomer.setMaterial(this);
        return this;
    }

    public Material removeIdmaterial(MaterialQuarryCustomer materialQuarryCustomer) {
        this.idmaterials.remove(materialQuarryCustomer);
        materialQuarryCustomer.setMaterial(null);
        return this;
    }

    public void setIdmaterials(Set<MaterialQuarryCustomer> materialQuarryCustomers) {
        this.idmaterials = materialQuarryCustomers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Material material = (Material) o;
        if (material.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), material.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Material{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
