package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Vehicle.
 */
@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "quantity", precision = 10, scale = 2)
    private BigDecimal quantity;

    @Column(name = "model")
    private String model;

    @ManyToOne
    @JsonIgnoreProperties("idcarriers")
    private Carrier carrier;

    @ManyToOne
    @JsonIgnoreProperties("idcapacities")
    private Capacity capacity;

    @ManyToOne
    @JsonIgnoreProperties("idbrands")
    private Brand brand;

    @OneToMany(mappedBy = "vehicle")
    private Set<Roadmap> idvehicles = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("vehicles")
    private Quarry quarry;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Vehicle licensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
        return this;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public Vehicle quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getModel() {
        return model;
    }

    public Vehicle model(String model) {
        this.model = model;
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public Vehicle carrier(Carrier carrier) {
        this.carrier = carrier;
        return this;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Capacity getCapacity() {
        return capacity;
    }

    public Vehicle capacity(Capacity capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Capacity capacity) {
        this.capacity = capacity;
    }

    public Brand getBrand() {
        return brand;
    }

    public Vehicle brand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Set<Roadmap> getIdvehicles() {
        return idvehicles;
    }

    public Vehicle idvehicles(Set<Roadmap> roadmaps) {
        this.idvehicles = roadmaps;
        return this;
    }

    public Vehicle addIdvehicle(Roadmap roadmap) {
        this.idvehicles.add(roadmap);
        roadmap.setVehicle(this);
        return this;
    }

    public Vehicle removeIdvehicle(Roadmap roadmap) {
        this.idvehicles.remove(roadmap);
        roadmap.setVehicle(null);
        return this;
    }

    public void setIdvehicles(Set<Roadmap> roadmaps) {
        this.idvehicles = roadmaps;
    }

    public Quarry getQuarry() {
        return quarry;
    }

    public Vehicle quarry(Quarry quarry) {
        this.quarry = quarry;
        return this;
    }

    public void setQuarry(Quarry quarry) {
        this.quarry = quarry;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        if (vehicle.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vehicle.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Vehicle{" +
            "id=" + getId() +
            ", licensePlate='" + getLicensePlate() + "'" +
            ", quantity=" + getQuantity() +
            ", model='" + getModel() + "'" +
            "}";
    }
}
