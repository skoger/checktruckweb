package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Carrier.
 */
@Entity
@Table(name = "carrier")
public class Carrier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "business_name", nullable = false)
    private String businessName;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "contact")
    private String contact;

    @Column(name = "contact_phone")
    private String contactPhone;

    @OneToMany(mappedBy = "carrier")
    private Set<Vehicle> idcarriers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public Carrier businessName(String businessName) {
        this.businessName = businessName;
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public Carrier address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public Carrier phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public Carrier contact(String contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public Carrier contactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
        return this;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public Set<Vehicle> getIdcarriers() {
        return idcarriers;
    }

    public Carrier idcarriers(Set<Vehicle> vehicles) {
        this.idcarriers = vehicles;
        return this;
    }

    public Carrier addIdcarrier(Vehicle vehicle) {
        this.idcarriers.add(vehicle);
        vehicle.setCarrier(this);
        return this;
    }

    public Carrier removeIdcarrier(Vehicle vehicle) {
        this.idcarriers.remove(vehicle);
        vehicle.setCarrier(null);
        return this;
    }

    public void setIdcarriers(Set<Vehicle> vehicles) {
        this.idcarriers = vehicles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Carrier carrier = (Carrier) o;
        if (carrier.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carrier.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Carrier{" +
            "id=" + getId() +
            ", businessName='" + getBusinessName() + "'" +
            ", address='" + getAddress() + "'" +
            ", phone='" + getPhone() + "'" +
            ", contact='" + getContact() + "'" +
            ", contactPhone='" + getContactPhone() + "'" +
            "}";
    }
}
