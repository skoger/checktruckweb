package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Routers.
 */
@Entity
@Table(name = "routers")
public class Routers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "distance_km", precision = 10, scale = 2)
    private BigDecimal distanceKm;

    @NotNull
    @Column(name = "jhi_cost", precision = 10, scale = 2, nullable = false)
    private BigDecimal cost;

    @NotNull
    @Column(name = "report_copies", nullable = false)
    private Integer reportCopies;

    @OneToMany(mappedBy = "routers")
    private Set<Roadmap> routersRaodmaps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Routers description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDistanceKm() {
        return distanceKm;
    }

    public Routers distanceKm(BigDecimal distanceKm) {
        this.distanceKm = distanceKm;
        return this;
    }

    public void setDistanceKm(BigDecimal distanceKm) {
        this.distanceKm = distanceKm;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public Routers cost(BigDecimal cost) {
        this.cost = cost;
        return this;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getReportCopies() {
        return reportCopies;
    }

    public Routers reportCopies(Integer reportCopies) {
        this.reportCopies = reportCopies;
        return this;
    }

    public void setReportCopies(Integer reportCopies) {
        this.reportCopies = reportCopies;
    }

    public Set<Roadmap> getRoutersRaodmaps() {
        return routersRaodmaps;
    }

    public Routers routersRaodmaps(Set<Roadmap> roadmaps) {
        this.routersRaodmaps = roadmaps;
        return this;
    }

    public Routers addRoutersRaodmap(Roadmap roadmap) {
        this.routersRaodmaps.add(roadmap);
        roadmap.setRouters(this);
        return this;
    }

    public Routers removeRoutersRaodmap(Roadmap roadmap) {
        this.routersRaodmaps.remove(roadmap);
        roadmap.setRouters(null);
        return this;
    }

    public void setRoutersRaodmaps(Set<Roadmap> roadmaps) {
        this.routersRaodmaps = roadmaps;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Routers routers = (Routers) o;
        if (routers.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), routers.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Routers{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", distanceKm=" + getDistanceKm() +
            ", cost=" + getCost() +
            ", reportCopies=" + getReportCopies() +
            "}";
    }
}
