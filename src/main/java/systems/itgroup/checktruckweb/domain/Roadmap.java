package systems.itgroup.checktruckweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Roadmap.
 */
@Entity
@Table(name = "roadmap")
public class Roadmap implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "price", precision = 10, scale = 2)
    private BigDecimal price;

    @Column(name = "synch")
    private Boolean synch;

    @Column(name = "verified")
    private Boolean verified;

    @Column(name = "id_user")
    private String idUser;

    @Column(name = "created")
    private ZonedDateTime created;

    @Column(name = "verified_date")
    private ZonedDateTime verifiedDate;

    @Column(name = "user_key")
    private String userKey;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "annulled")
    private Boolean annulled;

    @ManyToOne
    @JsonIgnoreProperties("routersRaodmaps")
    private Routers routers;

    @ManyToOne
    @JsonIgnoreProperties("idvehicles")
    private Vehicle vehicle;

    @ManyToOne
    @JsonIgnoreProperties("idmaterialquarries")
    private MaterialQuarryCustomer materialQuarryCustomer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Roadmap price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean isSynch() {
        return synch;
    }

    public Roadmap synch(Boolean synch) {
        this.synch = synch;
        return this;
    }

    public void setSynch(Boolean synch) {
        this.synch = synch;
    }

    public Boolean isVerified() {
        return verified;
    }

    public Roadmap verified(Boolean verified) {
        this.verified = verified;
        return this;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getIdUser() {
        return idUser;
    }

    public Roadmap idUser(String idUser) {
        this.idUser = idUser;
        return this;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public Roadmap created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getVerifiedDate() {
        return verifiedDate;
    }

    public Roadmap verifiedDate(ZonedDateTime verifiedDate) {
        this.verifiedDate = verifiedDate;
        return this;
    }

    public void setVerifiedDate(ZonedDateTime verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public String getUserKey() {
        return userKey;
    }

    public Roadmap userKey(String userKey) {
        this.userKey = userKey;
        return this;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserName() {
        return userName;
    }

    public Roadmap userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean isAnnulled() {
        return annulled;
    }

    public Roadmap annulled(Boolean annulled) {
        this.annulled = annulled;
        return this;
    }

    public void setAnnulled(Boolean annulled) {
        this.annulled = annulled;
    }

    public Routers getRouters() {
        return routers;
    }

    public Roadmap routers(Routers routers) {
        this.routers = routers;
        return this;
    }

    public void setRouters(Routers routers) {
        this.routers = routers;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public Roadmap vehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public MaterialQuarryCustomer getMaterialQuarryCustomer() {
        return materialQuarryCustomer;
    }

    public Roadmap materialQuarryCustomer(MaterialQuarryCustomer materialQuarryCustomer) {
        this.materialQuarryCustomer = materialQuarryCustomer;
        return this;
    }

    public void setMaterialQuarryCustomer(MaterialQuarryCustomer materialQuarryCustomer) {
        this.materialQuarryCustomer = materialQuarryCustomer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Roadmap roadmap = (Roadmap) o;
        if (roadmap.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roadmap.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Roadmap{" +
            "id=" + getId() +
            ", price=" + getPrice() +
            ", synch='" + isSynch() + "'" +
            ", verified='" + isVerified() + "'" +
            ", idUser='" + getIdUser() + "'" +
            ", created='" + getCreated() + "'" +
            ", verifiedDate='" + getVerifiedDate() + "'" +
            ", userKey='" + getUserKey() + "'" +
            ", userName='" + getUserName() + "'" +
            ", annulled='" + isAnnulled() + "'" +
            "}";
    }
}
