package systems.itgroup.checktruckweb.web.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@RestController
@RequestMapping("/v1/reports")
public class ReportResource implements Serializable {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private Connection con;
    private HttpHeaders headers;
    private ClassLoader classLoader = getClass().getClassLoader();
    private Map<String, Object> parameters;

    public ReportResource() {
        headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
    }

    @GetMapping("/printQR/{id}")
    public ResponseEntity<Resource> reportPDF(@PathVariable Long id) throws IOException, JRException {
        InputStream testReport = getClass().getResourceAsStream("/reports/qr_chectruck.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(testReport);

        parameters = new HashMap<>();
        parameters.put("IDVEHICLE", id);
        byte[] report = JasperRunManager.runReportToPdf(jasperReport, parameters, getConexion());
        ByteArrayResource resource = new ByteArrayResource(report);
        return ResponseEntity.ok().headers(headers)
                .contentType(MediaType.parseMediaType("application/pdf")).body(resource);
    }

    public Connection getConexion() {
        if (con == null) {
            try {
                con = jdbcTemplate.getDataSource().getConnection();
                System.out.println("Conexión establecida");
            } catch (SQLException e) {
                System.out.println("Error en la conexion: " + e.getSQLState());
            }
        }
        return con;
    }

}
