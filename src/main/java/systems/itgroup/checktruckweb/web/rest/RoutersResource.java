package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import systems.itgroup.checktruckweb.domain.Routers;
import systems.itgroup.checktruckweb.service.RoutersService;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;
import systems.itgroup.checktruckweb.service.dto.RoutersCriteria;
import systems.itgroup.checktruckweb.service.RoutersQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Routers.
 */
@RestController
@RequestMapping("/api")
public class RoutersResource {

    private final Logger log = LoggerFactory.getLogger(RoutersResource.class);

    private static final String ENTITY_NAME = "routers";

    private final RoutersService routersService;

    private final RoutersQueryService routersQueryService;

    public RoutersResource(RoutersService routersService, RoutersQueryService routersQueryService) {
        this.routersService = routersService;
        this.routersQueryService = routersQueryService;
    }

    /**
     * POST  /routers : Create a new routers.
     *
     * @param routers the routers to create
     * @return the ResponseEntity with status 201 (Created) and with body the new routers, or with status 400 (Bad Request) if the routers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/routers")
    @Timed
    public ResponseEntity<Routers> createRouters(@Valid @RequestBody Routers routers) throws URISyntaxException {
        log.debug("REST request to save Routers : {}", routers);
        if (routers.getId() != null) {
            throw new BadRequestAlertException("A new routers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Routers result = routersService.save(routers);
        return ResponseEntity.created(new URI("/api/routers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /routers : Updates an existing routers.
     *
     * @param routers the routers to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated routers,
     * or with status 400 (Bad Request) if the routers is not valid,
     * or with status 500 (Internal Server Error) if the routers couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/routers")
    @Timed
    public ResponseEntity<Routers> updateRouters(@Valid @RequestBody Routers routers) throws URISyntaxException {
        log.debug("REST request to update Routers : {}", routers);
        if (routers.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Routers result = routersService.save(routers);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, routers.getId().toString()))
            .body(result);
    }

    /**
     * GET  /routers : get all the routers.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of routers in body
     */
    @GetMapping("/routers")
    @Timed
    public ResponseEntity<List<Routers>> getAllRouters(RoutersCriteria criteria) {
        log.debug("REST request to get Routers by criteria: {}", criteria);
        List<Routers> entityList = routersQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /routers/:id : get the "id" routers.
     *
     * @param id the id of the routers to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the routers, or with status 404 (Not Found)
     */
    @GetMapping("/routers/{id}")
    @Timed
    public ResponseEntity<Routers> getRouters(@PathVariable Long id) {
        log.debug("REST request to get Routers : {}", id);
        Optional<Routers> routers = routersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(routers);
    }

    /**
     * DELETE  /routers/:id : delete the "id" routers.
     *
     * @param id the id of the routers to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/routers/{id}")
    @Timed
    public ResponseEntity<Void> deleteRouters(@PathVariable Long id) {
        log.debug("REST request to delete Routers : {}", id);
        routersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
