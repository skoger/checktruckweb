package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import systems.itgroup.checktruckweb.domain.MaterialQuarryCustomer;
import systems.itgroup.checktruckweb.service.MaterialQuarryCustomerService;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;
import systems.itgroup.checktruckweb.service.dto.MaterialQuarryCustomerCriteria;
import systems.itgroup.checktruckweb.service.MaterialQuarryCustomerQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MaterialQuarryCustomer.
 */
@RestController
@RequestMapping("/api")
public class MaterialQuarryCustomerResource {

    private final Logger log = LoggerFactory.getLogger(MaterialQuarryCustomerResource.class);

    private static final String ENTITY_NAME = "materialQuarryCustomer";

    private final MaterialQuarryCustomerService materialQuarryCustomerService;

    private final MaterialQuarryCustomerQueryService materialQuarryCustomerQueryService;

    public MaterialQuarryCustomerResource(MaterialQuarryCustomerService materialQuarryCustomerService, MaterialQuarryCustomerQueryService materialQuarryCustomerQueryService) {
        this.materialQuarryCustomerService = materialQuarryCustomerService;
        this.materialQuarryCustomerQueryService = materialQuarryCustomerQueryService;
    }

    /**
     * POST  /material-quarry-customers : Create a new materialQuarryCustomer.
     *
     * @param materialQuarryCustomer the materialQuarryCustomer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new materialQuarryCustomer, or with status 400 (Bad Request) if the materialQuarryCustomer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/material-quarry-customers")
    @Timed
    public ResponseEntity<MaterialQuarryCustomer> createMaterialQuarryCustomer(@Valid @RequestBody MaterialQuarryCustomer materialQuarryCustomer) throws URISyntaxException {
        log.debug("REST request to save MaterialQuarryCustomer : {}", materialQuarryCustomer);
        if (materialQuarryCustomer.getId() != null) {
            throw new BadRequestAlertException("A new materialQuarryCustomer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialQuarryCustomer result = materialQuarryCustomerService.save(materialQuarryCustomer);
        return ResponseEntity.created(new URI("/api/material-quarry-customers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /material-quarry-customers : Updates an existing materialQuarryCustomer.
     *
     * @param materialQuarryCustomer the materialQuarryCustomer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated materialQuarryCustomer,
     * or with status 400 (Bad Request) if the materialQuarryCustomer is not valid,
     * or with status 500 (Internal Server Error) if the materialQuarryCustomer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/material-quarry-customers")
    @Timed
    public ResponseEntity<MaterialQuarryCustomer> updateMaterialQuarryCustomer(@Valid @RequestBody MaterialQuarryCustomer materialQuarryCustomer) throws URISyntaxException {
        log.debug("REST request to update MaterialQuarryCustomer : {}", materialQuarryCustomer);
        if (materialQuarryCustomer.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialQuarryCustomer result = materialQuarryCustomerService.save(materialQuarryCustomer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, materialQuarryCustomer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /material-quarry-customers : get all the materialQuarryCustomers.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of materialQuarryCustomers in body
     */
    @GetMapping("/material-quarry-customers")
    @Timed
    public ResponseEntity<List<MaterialQuarryCustomer>> getAllMaterialQuarryCustomers(MaterialQuarryCustomerCriteria criteria) {
        log.debug("REST request to get MaterialQuarryCustomers by criteria: {}", criteria);
        List<MaterialQuarryCustomer> entityList = materialQuarryCustomerQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /material-quarry-customers/:id : get the "id" materialQuarryCustomer.
     *
     * @param id the id of the materialQuarryCustomer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the materialQuarryCustomer, or with status 404 (Not Found)
     */
    @GetMapping("/material-quarry-customers/{id}")
    @Timed
    public ResponseEntity<MaterialQuarryCustomer> getMaterialQuarryCustomer(@PathVariable Long id) {
        log.debug("REST request to get MaterialQuarryCustomer : {}", id);
        Optional<MaterialQuarryCustomer> materialQuarryCustomer = materialQuarryCustomerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialQuarryCustomer);
    }

    /**
     * DELETE  /material-quarry-customers/:id : delete the "id" materialQuarryCustomer.
     *
     * @param id the id of the materialQuarryCustomer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/material-quarry-customers/{id}")
    @Timed
    public ResponseEntity<Void> deleteMaterialQuarryCustomer(@PathVariable Long id) {
        log.debug("REST request to delete MaterialQuarryCustomer : {}", id);
        materialQuarryCustomerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
