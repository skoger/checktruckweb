package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.service.ITCheckTruckService;
import systems.itgroup.checktruckweb.service.dto.RoadmapDTO;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * REST controller for managing Carrier.
 */
@RestController
@RequestMapping("/api")
public class ITCheckTruckResource {
    private final Logger log = LoggerFactory.getLogger(CarrierResource.class);

    @Autowired
    private ITCheckTruckService itCheckTruckService;
    /**
     * POST  /roadmap : Create a new carrier.
     *
     * @param roadmapDTO the roadmapDto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carrier, or with status 400 (Bad Request) if the carrier has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/verification")
    @Timed
    public ResponseEntity<Roadmap> verification(@Valid @RequestBody RoadmapDTO roadmapDTO) throws URISyntaxException {
        log.debug("REST request to save roadmap : {}", roadmapDTO);
//        if (carrier.getId() != null) {
//            throw new BadRequestAlertException("A new carrier cannot already have an ID", ENTITY_NAME, "idexists");
//        }
        try {
            Roadmap result = itCheckTruckService.verification(roadmapDTO);
            return ResponseEntity.created(new URI("/api/verification/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("Roadmap", result.getId().toString()))
                .body(result);
        }catch (Exception e){
            throw new BadRequestAlertException("The roadmap already exists and is verified", "Roadmap", "idverified");

        }
    }
}
