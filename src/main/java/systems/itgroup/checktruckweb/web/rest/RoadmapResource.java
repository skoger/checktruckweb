package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import systems.itgroup.checktruckweb.domain.Roadmap;
import systems.itgroup.checktruckweb.service.RoadmapService;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;
import systems.itgroup.checktruckweb.service.dto.RoadmapCriteria;
import systems.itgroup.checktruckweb.service.RoadmapQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Roadmap.
 */
@RestController
@RequestMapping("/api")
public class RoadmapResource {

    private final Logger log = LoggerFactory.getLogger(RoadmapResource.class);

    private static final String ENTITY_NAME = "roadmap";

    private final RoadmapService roadmapService;

    private final RoadmapQueryService roadmapQueryService;

    public RoadmapResource(RoadmapService roadmapService, RoadmapQueryService roadmapQueryService) {
        this.roadmapService = roadmapService;
        this.roadmapQueryService = roadmapQueryService;
    }

    /**
     * POST  /roadmaps : Create a new roadmap.
     *
     * @param roadmap the roadmap to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roadmap, or with status 400 (Bad Request) if the roadmap has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/roadmaps")
    @Timed
    public ResponseEntity<Roadmap> createRoadmap(@RequestBody Roadmap roadmap) throws URISyntaxException {
        log.debug("REST request to save Roadmap : {}", roadmap);
        if (roadmap.getId() != null) {
            throw new BadRequestAlertException("A new roadmap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Roadmap result = roadmapService.save(roadmap);
        return ResponseEntity.created(new URI("/api/roadmaps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /roadmaps : Updates an existing roadmap.
     *
     * @param roadmap the roadmap to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roadmap,
     * or with status 400 (Bad Request) if the roadmap is not valid,
     * or with status 500 (Internal Server Error) if the roadmap couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/roadmaps")
    @Timed
    public ResponseEntity<Roadmap> updateRoadmap(@RequestBody Roadmap roadmap) throws URISyntaxException {
        log.debug("REST request to update Roadmap : {}", roadmap);
        if (roadmap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Roadmap result = roadmapService.save(roadmap);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roadmap.getId().toString()))
            .body(result);
    }

    /**
     * GET  /roadmaps : get all the roadmaps.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of roadmaps in body
     */
    @GetMapping("/roadmaps")
    @Timed
    public ResponseEntity<List<Roadmap>> getAllRoadmaps(RoadmapCriteria criteria) {
        log.debug("REST request to get Roadmaps by criteria: {}", criteria);
        List<Roadmap> entityList = roadmapQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /roadmaps/:id : get the "id" roadmap.
     *
     * @param id the id of the roadmap to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roadmap, or with status 404 (Not Found)
     */
    @GetMapping("/roadmaps/{id}")
    @Timed
    public ResponseEntity<Roadmap> getRoadmap(@PathVariable Long id) {
        log.debug("REST request to get Roadmap : {}", id);
        Optional<Roadmap> roadmap = roadmapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roadmap);
    }

    /**
     * DELETE  /roadmaps/:id : delete the "id" roadmap.
     *
     * @param id the id of the roadmap to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/roadmaps/{id}")
    @Timed
    public ResponseEntity<Void> deleteRoadmap(@PathVariable Long id) {
        log.debug("REST request to delete Roadmap : {}", id);
        roadmapService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
