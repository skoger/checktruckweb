/**
 * View Models used by Spring MVC REST controllers.
 */
package systems.itgroup.checktruckweb.web.rest.vm;
