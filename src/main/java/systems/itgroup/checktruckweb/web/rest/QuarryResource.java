package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import systems.itgroup.checktruckweb.domain.Quarry;
import systems.itgroup.checktruckweb.service.QuarryService;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;
import systems.itgroup.checktruckweb.service.dto.QuarryCriteria;
import systems.itgroup.checktruckweb.service.QuarryQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Quarry.
 */
@RestController
@RequestMapping("/api")
public class QuarryResource {

    private final Logger log = LoggerFactory.getLogger(QuarryResource.class);

    private static final String ENTITY_NAME = "quarry";

    private final QuarryService quarryService;

    private final QuarryQueryService quarryQueryService;

    public QuarryResource(QuarryService quarryService, QuarryQueryService quarryQueryService) {
        this.quarryService = quarryService;
        this.quarryQueryService = quarryQueryService;
    }

    /**
     * POST  /quarries : Create a new quarry.
     *
     * @param quarry the quarry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new quarry, or with status 400 (Bad Request) if the quarry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quarries")
    @Timed
    public ResponseEntity<Quarry> createQuarry(@Valid @RequestBody Quarry quarry) throws URISyntaxException {
        log.debug("REST request to save Quarry : {}", quarry);
        if (quarry.getId() != null) {
            throw new BadRequestAlertException("A new quarry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Quarry result = quarryService.save(quarry);
        return ResponseEntity.created(new URI("/api/quarries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /quarries : Updates an existing quarry.
     *
     * @param quarry the quarry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated quarry,
     * or with status 400 (Bad Request) if the quarry is not valid,
     * or with status 500 (Internal Server Error) if the quarry couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/quarries")
    @Timed
    public ResponseEntity<Quarry> updateQuarry(@Valid @RequestBody Quarry quarry) throws URISyntaxException {
        log.debug("REST request to update Quarry : {}", quarry);
        if (quarry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Quarry result = quarryService.save(quarry);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, quarry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /quarries : get all the quarries.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of quarries in body
     */
    @GetMapping("/quarries")
    @Timed
    public ResponseEntity<List<Quarry>> getAllQuarries(QuarryCriteria criteria) {
        log.debug("REST request to get Quarries by criteria: {}", criteria);
        List<Quarry> entityList = quarryQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /quarries/:id : get the "id" quarry.
     *
     * @param id the id of the quarry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the quarry, or with status 404 (Not Found)
     */
    @GetMapping("/quarries/{id}")
    @Timed
    public ResponseEntity<Quarry> getQuarry(@PathVariable Long id) {
        log.debug("REST request to get Quarry : {}", id);
        Optional<Quarry> quarry = quarryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quarry);
    }

    /**
     * DELETE  /quarries/:id : delete the "id" quarry.
     *
     * @param id the id of the quarry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/quarries/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuarry(@PathVariable Long id) {
        log.debug("REST request to delete Quarry : {}", id);
        quarryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
