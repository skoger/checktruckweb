package systems.itgroup.checktruckweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import systems.itgroup.checktruckweb.domain.Capacity;
import systems.itgroup.checktruckweb.service.CapacityService;
import systems.itgroup.checktruckweb.web.rest.errors.BadRequestAlertException;
import systems.itgroup.checktruckweb.web.rest.util.HeaderUtil;
import systems.itgroup.checktruckweb.service.dto.CapacityCriteria;
import systems.itgroup.checktruckweb.service.CapacityQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Capacity.
 */
@RestController
@RequestMapping("/api")
public class CapacityResource {

    private final Logger log = LoggerFactory.getLogger(CapacityResource.class);

    private static final String ENTITY_NAME = "capacity";

    private final CapacityService capacityService;

    private final CapacityQueryService capacityQueryService;

    public CapacityResource(CapacityService capacityService, CapacityQueryService capacityQueryService) {
        this.capacityService = capacityService;
        this.capacityQueryService = capacityQueryService;
    }

    /**
     * POST  /capacities : Create a new capacity.
     *
     * @param capacity the capacity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new capacity, or with status 400 (Bad Request) if the capacity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/capacities")
    @Timed
    public ResponseEntity<Capacity> createCapacity(@Valid @RequestBody Capacity capacity) throws URISyntaxException {
        log.debug("REST request to save Capacity : {}", capacity);
        if (capacity.getId() != null) {
            throw new BadRequestAlertException("A new capacity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Capacity result = capacityService.save(capacity);
        return ResponseEntity.created(new URI("/api/capacities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /capacities : Updates an existing capacity.
     *
     * @param capacity the capacity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated capacity,
     * or with status 400 (Bad Request) if the capacity is not valid,
     * or with status 500 (Internal Server Error) if the capacity couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/capacities")
    @Timed
    public ResponseEntity<Capacity> updateCapacity(@Valid @RequestBody Capacity capacity) throws URISyntaxException {
        log.debug("REST request to update Capacity : {}", capacity);
        if (capacity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Capacity result = capacityService.save(capacity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, capacity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /capacities : get all the capacities.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of capacities in body
     */
    @GetMapping("/capacities")
    @Timed
    public ResponseEntity<List<Capacity>> getAllCapacities(CapacityCriteria criteria) {
        log.debug("REST request to get Capacities by criteria: {}", criteria);
        List<Capacity> entityList = capacityQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /capacities/:id : get the "id" capacity.
     *
     * @param id the id of the capacity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the capacity, or with status 404 (Not Found)
     */
    @GetMapping("/capacities/{id}")
    @Timed
    public ResponseEntity<Capacity> getCapacity(@PathVariable Long id) {
        log.debug("REST request to get Capacity : {}", id);
        Optional<Capacity> capacity = capacityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(capacity);
    }

    /**
     * DELETE  /capacities/:id : delete the "id" capacity.
     *
     * @param id the id of the capacity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/capacities/{id}")
    @Timed
    public ResponseEntity<Void> deleteCapacity(@PathVariable Long id) {
        log.debug("REST request to delete Capacity : {}", id);
        capacityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
