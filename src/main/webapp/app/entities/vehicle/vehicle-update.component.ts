import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IVehicle } from 'app/shared/model/vehicle.model';
import { VehicleService } from './vehicle.service';
import { ICarrier } from 'app/shared/model/carrier.model';
import { CarrierService } from 'app/entities/carrier';
import { ICapacity } from 'app/shared/model/capacity.model';
import { CapacityService } from 'app/entities/capacity';
import { IBrand } from 'app/shared/model/brand.model';
import { BrandService } from 'app/entities/brand';
import { IQuarry } from 'app/shared/model/quarry.model';
import { QuarryService } from 'app/entities/quarry';

@Component({
    selector: 'jhi-vehicle-update',
    templateUrl: './vehicle-update.component.html'
})
export class VehicleUpdateComponent implements OnInit {
    private _vehicle: IVehicle;
    isSaving: boolean;

    carriers: ICarrier[];

    capacities: ICapacity[];

    brands: IBrand[];

    quarries: IQuarry[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private vehicleService: VehicleService,
        private carrierService: CarrierService,
        private capacityService: CapacityService,
        private brandService: BrandService,
        private quarryService: QuarryService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ vehicle }) => {
            this.vehicle = vehicle;
        });
        this.carrierService.query().subscribe(
            (res: HttpResponse<ICarrier[]>) => {
                this.carriers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.capacityService.query().subscribe(
            (res: HttpResponse<ICapacity[]>) => {
                this.capacities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.brandService.query().subscribe(
            (res: HttpResponse<IBrand[]>) => {
                this.brands = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.quarryService.query().subscribe(
            (res: HttpResponse<IQuarry[]>) => {
                this.quarries = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.vehicle.id !== undefined) {
            this.subscribeToSaveResponse(this.vehicleService.update(this.vehicle));
        } else {
            this.subscribeToSaveResponse(this.vehicleService.create(this.vehicle));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IVehicle>>) {
        result.subscribe((res: HttpResponse<IVehicle>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCarrierById(index: number, item: ICarrier) {
        return item.id;
    }

    trackCapacityById(index: number, item: ICapacity) {
        return item.id;
    }

    trackBrandById(index: number, item: IBrand) {
        return item.id;
    }

    trackQuarryById(index: number, item: IQuarry) {
        return item.id;
    }
    get vehicle() {
        return this._vehicle;
    }

    set vehicle(vehicle: IVehicle) {
        this._vehicle = vehicle;
    }
}
