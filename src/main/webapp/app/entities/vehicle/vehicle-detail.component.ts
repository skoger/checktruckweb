import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVehicle } from 'app/shared/model/vehicle.model';
import { SERVER_API_URL } from 'app/app.constants';

@Component({
    selector: 'jhi-vehicle-detail',
    templateUrl: './vehicle-detail.component.html'
})
export class VehicleDetailComponent implements OnInit {
    vehicle: IVehicle;
    routeQr: String = SERVER_API_URL + 'v1/reports/printQR/';

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ vehicle }) => {
            this.vehicle = vehicle;
            this.routeQr = this.routeQr + vehicle.id;
        });
    }

    previousState() {
        window.history.back();
    }
}
