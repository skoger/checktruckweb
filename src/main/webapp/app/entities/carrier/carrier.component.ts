import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICarrier } from 'app/shared/model/carrier.model';
import { Principal } from 'app/core';
import { CarrierService } from './carrier.service';

@Component({
    selector: 'jhi-carrier',
    templateUrl: './carrier.component.html'
})
export class CarrierComponent implements OnInit, OnDestroy {
    carriers: ICarrier[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private carrierService: CarrierService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.carrierService.query().subscribe(
            (res: HttpResponse<ICarrier[]>) => {
                this.carriers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCarriers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICarrier) {
        return item.id;
    }

    registerChangeInCarriers() {
        this.eventSubscriber = this.eventManager.subscribe('carrierListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
