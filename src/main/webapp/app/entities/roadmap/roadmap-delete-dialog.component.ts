import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRoadmap } from 'app/shared/model/roadmap.model';
import { RoadmapService } from './roadmap.service';

@Component({
    selector: 'jhi-roadmap-delete-dialog',
    templateUrl: './roadmap-delete-dialog.component.html'
})
export class RoadmapDeleteDialogComponent {
    roadmap: IRoadmap;

    constructor(private roadmapService: RoadmapService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.roadmapService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'roadmapListModification',
                content: 'Deleted an roadmap'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-roadmap-delete-popup',
    template: ''
})
export class RoadmapDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ roadmap }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RoadmapDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.roadmap = roadmap;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
