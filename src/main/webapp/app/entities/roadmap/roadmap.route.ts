import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Roadmap } from 'app/shared/model/roadmap.model';
import { RoadmapService } from './roadmap.service';
import { RoadmapComponent } from './roadmap.component';
import { RoadmapDetailComponent } from './roadmap-detail.component';
import { RoadmapUpdateComponent } from './roadmap-update.component';
import { RoadmapDeletePopupComponent } from './roadmap-delete-dialog.component';
import { IRoadmap } from 'app/shared/model/roadmap.model';

@Injectable({ providedIn: 'root' })
export class RoadmapResolve implements Resolve<IRoadmap> {
    constructor(private service: RoadmapService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((roadmap: HttpResponse<Roadmap>) => roadmap.body));
        }
        return of(new Roadmap());
    }
}

export const roadmapRoute: Routes = [
    {
        path: 'roadmap',
        component: RoadmapComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.roadmap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'roadmap/:id/view',
        component: RoadmapDetailComponent,
        resolve: {
            roadmap: RoadmapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.roadmap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'roadmap/new',
        component: RoadmapUpdateComponent,
        resolve: {
            roadmap: RoadmapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.roadmap.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'roadmap/:id/edit',
        component: RoadmapUpdateComponent,
        resolve: {
            roadmap: RoadmapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.roadmap.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const roadmapPopupRoute: Routes = [
    {
        path: 'roadmap/:id/delete',
        component: RoadmapDeletePopupComponent,
        resolve: {
            roadmap: RoadmapResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.roadmap.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
