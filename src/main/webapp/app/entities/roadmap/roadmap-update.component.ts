import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IRoadmap } from 'app/shared/model/roadmap.model';
import { RoadmapService } from './roadmap.service';
import { IRouters } from 'app/shared/model/routers.model';
import { RoutersService } from 'app/entities/routers';
import { IVehicle } from 'app/shared/model/vehicle.model';
import { VehicleService } from 'app/entities/vehicle';
import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';
import { MaterialQuarryCustomerService } from 'app/entities/material-quarry-customer';

@Component({
    selector: 'jhi-roadmap-update',
    templateUrl: './roadmap-update.component.html'
})
export class RoadmapUpdateComponent implements OnInit {
    private _roadmap: IRoadmap;
    isSaving: boolean;

    routers: IRouters[];

    vehicles: IVehicle[];

    materialquarrycustomers: IMaterialQuarryCustomer[];
    created: string;
    verifiedDate: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private roadmapService: RoadmapService,
        private routersService: RoutersService,
        private vehicleService: VehicleService,
        private materialQuarryCustomerService: MaterialQuarryCustomerService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ roadmap }) => {
            this.roadmap = roadmap;
        });
        this.routersService.query().subscribe(
            (res: HttpResponse<IRouters[]>) => {
                this.routers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.vehicleService.query().subscribe(
            (res: HttpResponse<IVehicle[]>) => {
                this.vehicles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.materialQuarryCustomerService.query().subscribe(
            (res: HttpResponse<IMaterialQuarryCustomer[]>) => {
                this.materialquarrycustomers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.roadmap.created = moment(this.created, DATE_TIME_FORMAT);
        this.roadmap.verifiedDate = moment(this.verifiedDate, DATE_TIME_FORMAT);
        if (this.roadmap.id !== undefined) {
            this.subscribeToSaveResponse(this.roadmapService.update(this.roadmap));
        } else {
            this.subscribeToSaveResponse(this.roadmapService.create(this.roadmap));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRoadmap>>) {
        result.subscribe((res: HttpResponse<IRoadmap>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRoutersById(index: number, item: IRouters) {
        return item.id;
    }

    trackVehicleById(index: number, item: IVehicle) {
        return item.id;
    }

    trackMaterialQuarryCustomerById(index: number, item: IMaterialQuarryCustomer) {
        return item.id;
    }
    get roadmap() {
        return this._roadmap;
    }

    set roadmap(roadmap: IRoadmap) {
        this._roadmap = roadmap;
        this.created = moment(roadmap.created).format(DATE_TIME_FORMAT);
        this.verifiedDate = moment(roadmap.verifiedDate).format(DATE_TIME_FORMAT);
    }
}
