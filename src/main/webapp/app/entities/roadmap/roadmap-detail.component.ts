import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRoadmap } from 'app/shared/model/roadmap.model';

@Component({
    selector: 'jhi-roadmap-detail',
    templateUrl: './roadmap-detail.component.html'
})
export class RoadmapDetailComponent implements OnInit {
    roadmap: IRoadmap;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ roadmap }) => {
            this.roadmap = roadmap;
        });
    }

    previousState() {
        window.history.back();
    }
}
