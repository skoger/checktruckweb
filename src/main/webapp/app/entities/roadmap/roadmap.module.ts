import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChecktruckwebSharedModule } from 'app/shared';
import {
    RoadmapComponent,
    RoadmapDetailComponent,
    RoadmapUpdateComponent,
    RoadmapDeletePopupComponent,
    RoadmapDeleteDialogComponent,
    roadmapRoute,
    roadmapPopupRoute
} from './';

const ENTITY_STATES = [...roadmapRoute, ...roadmapPopupRoute];

@NgModule({
    imports: [ChecktruckwebSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RoadmapComponent,
        RoadmapDetailComponent,
        RoadmapUpdateComponent,
        RoadmapDeleteDialogComponent,
        RoadmapDeletePopupComponent
    ],
    entryComponents: [RoadmapComponent, RoadmapUpdateComponent, RoadmapDeleteDialogComponent, RoadmapDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebRoadmapModule {}
