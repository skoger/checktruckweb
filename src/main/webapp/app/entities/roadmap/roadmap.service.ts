import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRoadmap } from 'app/shared/model/roadmap.model';

type EntityResponseType = HttpResponse<IRoadmap>;
type EntityArrayResponseType = HttpResponse<IRoadmap[]>;

@Injectable({ providedIn: 'root' })
export class RoadmapService {
    private resourceUrl = SERVER_API_URL + 'api/roadmaps';

    constructor(private http: HttpClient) {}

    create(roadmap: IRoadmap): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(roadmap);
        return this.http
            .post<IRoadmap>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(roadmap: IRoadmap): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(roadmap);
        return this.http
            .put<IRoadmap>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IRoadmap>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IRoadmap[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(roadmap: IRoadmap): IRoadmap {
        const copy: IRoadmap = Object.assign({}, roadmap, {
            created: roadmap.created != null && roadmap.created.isValid() ? roadmap.created.toJSON() : null,
            verifiedDate: roadmap.verifiedDate != null && roadmap.verifiedDate.isValid() ? roadmap.verifiedDate.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.created = res.body.created != null ? moment(res.body.created) : null;
        res.body.verifiedDate = res.body.verifiedDate != null ? moment(res.body.verifiedDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((roadmap: IRoadmap) => {
            roadmap.created = roadmap.created != null ? moment(roadmap.created) : null;
            roadmap.verifiedDate = roadmap.verifiedDate != null ? moment(roadmap.verifiedDate) : null;
        });
        return res;
    }
}
