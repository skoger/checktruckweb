import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRoadmap } from 'app/shared/model/roadmap.model';
import { Principal } from 'app/core';
import { RoadmapService } from './roadmap.service';

@Component({
    selector: 'jhi-roadmap',
    templateUrl: './roadmap.component.html'
})
export class RoadmapComponent implements OnInit, OnDestroy {
    roadmaps: IRoadmap[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private roadmapService: RoadmapService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.roadmapService.query().subscribe(
            (res: HttpResponse<IRoadmap[]>) => {
                this.roadmaps = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRoadmaps();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRoadmap) {
        return item.id;
    }

    registerChangeInRoadmaps() {
        this.eventSubscriber = this.eventManager.subscribe('roadmapListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
