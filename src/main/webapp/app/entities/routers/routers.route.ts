import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Routers } from 'app/shared/model/routers.model';
import { RoutersService } from './routers.service';
import { RoutersComponent } from './routers.component';
import { RoutersDetailComponent } from './routers-detail.component';
import { RoutersUpdateComponent } from './routers-update.component';
import { RoutersDeletePopupComponent } from './routers-delete-dialog.component';
import { IRouters } from 'app/shared/model/routers.model';

@Injectable({ providedIn: 'root' })
export class RoutersResolve implements Resolve<IRouters> {
    constructor(private service: RoutersService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((routers: HttpResponse<Routers>) => routers.body));
        }
        return of(new Routers());
    }
}

export const routersRoute: Routes = [
    {
        path: 'routers',
        component: RoutersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.routers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'routers/:id/view',
        component: RoutersDetailComponent,
        resolve: {
            routers: RoutersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.routers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'routers/new',
        component: RoutersUpdateComponent,
        resolve: {
            routers: RoutersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.routers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'routers/:id/edit',
        component: RoutersUpdateComponent,
        resolve: {
            routers: RoutersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.routers.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const routersPopupRoute: Routes = [
    {
        path: 'routers/:id/delete',
        component: RoutersDeletePopupComponent,
        resolve: {
            routers: RoutersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.routers.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
