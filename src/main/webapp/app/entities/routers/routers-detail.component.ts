import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRouters } from 'app/shared/model/routers.model';

@Component({
    selector: 'jhi-routers-detail',
    templateUrl: './routers-detail.component.html'
})
export class RoutersDetailComponent implements OnInit {
    routers: IRouters;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ routers }) => {
            this.routers = routers;
        });
    }

    previousState() {
        window.history.back();
    }
}
