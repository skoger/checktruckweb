import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IRouters } from 'app/shared/model/routers.model';
import { RoutersService } from './routers.service';

@Component({
    selector: 'jhi-routers-update',
    templateUrl: './routers-update.component.html'
})
export class RoutersUpdateComponent implements OnInit {
    private _routers: IRouters;
    isSaving: boolean;

    constructor(private routersService: RoutersService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ routers }) => {
            this.routers = routers;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.routers.id !== undefined) {
            this.subscribeToSaveResponse(this.routersService.update(this.routers));
        } else {
            this.subscribeToSaveResponse(this.routersService.create(this.routers));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRouters>>) {
        result.subscribe((res: HttpResponse<IRouters>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get routers() {
        return this._routers;
    }

    set routers(routers: IRouters) {
        this._routers = routers;
    }
}
