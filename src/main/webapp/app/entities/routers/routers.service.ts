import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRouters } from 'app/shared/model/routers.model';

type EntityResponseType = HttpResponse<IRouters>;
type EntityArrayResponseType = HttpResponse<IRouters[]>;

@Injectable({ providedIn: 'root' })
export class RoutersService {
    private resourceUrl = SERVER_API_URL + 'api/routers';

    constructor(private http: HttpClient) {}

    create(routers: IRouters): Observable<EntityResponseType> {
        return this.http.post<IRouters>(this.resourceUrl, routers, { observe: 'response' });
    }

    update(routers: IRouters): Observable<EntityResponseType> {
        return this.http.put<IRouters>(this.resourceUrl, routers, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRouters>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRouters[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
