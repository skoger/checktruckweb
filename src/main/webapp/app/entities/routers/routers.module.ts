import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChecktruckwebSharedModule } from 'app/shared';
import {
    RoutersComponent,
    RoutersDetailComponent,
    RoutersUpdateComponent,
    RoutersDeletePopupComponent,
    RoutersDeleteDialogComponent,
    routersRoute,
    routersPopupRoute
} from './';

const ENTITY_STATES = [...routersRoute, ...routersPopupRoute];

@NgModule({
    imports: [ChecktruckwebSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RoutersComponent,
        RoutersDetailComponent,
        RoutersUpdateComponent,
        RoutersDeleteDialogComponent,
        RoutersDeletePopupComponent
    ],
    entryComponents: [RoutersComponent, RoutersUpdateComponent, RoutersDeleteDialogComponent, RoutersDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebRoutersModule {}
