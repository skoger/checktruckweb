import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRouters } from 'app/shared/model/routers.model';
import { Principal } from 'app/core';
import { RoutersService } from './routers.service';

@Component({
    selector: 'jhi-routers',
    templateUrl: './routers.component.html'
})
export class RoutersComponent implements OnInit, OnDestroy {
    routers: IRouters[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private routersService: RoutersService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.routersService.query().subscribe(
            (res: HttpResponse<IRouters[]>) => {
                this.routers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRouters();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRouters) {
        return item.id;
    }

    registerChangeInRouters() {
        this.eventSubscriber = this.eventManager.subscribe('routersListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
