import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChecktruckwebSharedModule } from 'app/shared';
import {
    CapacityComponent,
    CapacityDetailComponent,
    CapacityUpdateComponent,
    CapacityDeletePopupComponent,
    CapacityDeleteDialogComponent,
    capacityRoute,
    capacityPopupRoute
} from './';

const ENTITY_STATES = [...capacityRoute, ...capacityPopupRoute];

@NgModule({
    imports: [ChecktruckwebSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CapacityComponent,
        CapacityDetailComponent,
        CapacityUpdateComponent,
        CapacityDeleteDialogComponent,
        CapacityDeletePopupComponent
    ],
    entryComponents: [CapacityComponent, CapacityUpdateComponent, CapacityDeleteDialogComponent, CapacityDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebCapacityModule {}
