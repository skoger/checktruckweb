import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICapacity } from 'app/shared/model/capacity.model';
import { CapacityService } from './capacity.service';

@Component({
    selector: 'jhi-capacity-update',
    templateUrl: './capacity-update.component.html'
})
export class CapacityUpdateComponent implements OnInit {
    private _capacity: ICapacity;
    isSaving: boolean;

    constructor(private capacityService: CapacityService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ capacity }) => {
            this.capacity = capacity;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.capacity.id !== undefined) {
            this.subscribeToSaveResponse(this.capacityService.update(this.capacity));
        } else {
            this.subscribeToSaveResponse(this.capacityService.create(this.capacity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICapacity>>) {
        result.subscribe((res: HttpResponse<ICapacity>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get capacity() {
        return this._capacity;
    }

    set capacity(capacity: ICapacity) {
        this._capacity = capacity;
    }
}
