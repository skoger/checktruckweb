import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICapacity } from 'app/shared/model/capacity.model';

@Component({
    selector: 'jhi-capacity-detail',
    templateUrl: './capacity-detail.component.html'
})
export class CapacityDetailComponent implements OnInit {
    capacity: ICapacity;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ capacity }) => {
            this.capacity = capacity;
        });
    }

    previousState() {
        window.history.back();
    }
}
