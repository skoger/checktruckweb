import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICapacity } from 'app/shared/model/capacity.model';

type EntityResponseType = HttpResponse<ICapacity>;
type EntityArrayResponseType = HttpResponse<ICapacity[]>;

@Injectable({ providedIn: 'root' })
export class CapacityService {
    private resourceUrl = SERVER_API_URL + 'api/capacities';

    constructor(private http: HttpClient) {}

    create(capacity: ICapacity): Observable<EntityResponseType> {
        return this.http.post<ICapacity>(this.resourceUrl, capacity, { observe: 'response' });
    }

    update(capacity: ICapacity): Observable<EntityResponseType> {
        return this.http.put<ICapacity>(this.resourceUrl, capacity, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICapacity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICapacity[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
