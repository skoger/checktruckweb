import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Capacity } from 'app/shared/model/capacity.model';
import { CapacityService } from './capacity.service';
import { CapacityComponent } from './capacity.component';
import { CapacityDetailComponent } from './capacity-detail.component';
import { CapacityUpdateComponent } from './capacity-update.component';
import { CapacityDeletePopupComponent } from './capacity-delete-dialog.component';
import { ICapacity } from 'app/shared/model/capacity.model';

@Injectable({ providedIn: 'root' })
export class CapacityResolve implements Resolve<ICapacity> {
    constructor(private service: CapacityService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((capacity: HttpResponse<Capacity>) => capacity.body));
        }
        return of(new Capacity());
    }
}

export const capacityRoute: Routes = [
    {
        path: 'capacity',
        component: CapacityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.capacity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'capacity/:id/view',
        component: CapacityDetailComponent,
        resolve: {
            capacity: CapacityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.capacity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'capacity/new',
        component: CapacityUpdateComponent,
        resolve: {
            capacity: CapacityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.capacity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'capacity/:id/edit',
        component: CapacityUpdateComponent,
        resolve: {
            capacity: CapacityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.capacity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const capacityPopupRoute: Routes = [
    {
        path: 'capacity/:id/delete',
        component: CapacityDeletePopupComponent,
        resolve: {
            capacity: CapacityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.capacity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
