import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ChecktruckwebRoutersModule } from './routers/routers.module';
import { ChecktruckwebCarrierModule } from './carrier/carrier.module';
import { ChecktruckwebCapacityModule } from './capacity/capacity.module';
import { ChecktruckwebBrandModule } from './brand/brand.module';
import { ChecktruckwebVehicleModule } from './vehicle/vehicle.module';
import { ChecktruckwebCustomerModule } from './customer/customer.module';
import { ChecktruckwebQuarryModule } from './quarry/quarry.module';
import { ChecktruckwebMaterialModule } from './material/material.module';
import { ChecktruckwebMaterialQuarryCustomerModule } from './material-quarry-customer/material-quarry-customer.module';
import { ChecktruckwebRoadmapModule } from './roadmap/roadmap.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        ChecktruckwebRoutersModule,
        ChecktruckwebCarrierModule,
        ChecktruckwebCapacityModule,
        ChecktruckwebBrandModule,
        ChecktruckwebVehicleModule,
        ChecktruckwebCustomerModule,
        ChecktruckwebQuarryModule,
        ChecktruckwebMaterialModule,
        ChecktruckwebMaterialQuarryCustomerModule,
        ChecktruckwebRoadmapModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebEntityModule {}
