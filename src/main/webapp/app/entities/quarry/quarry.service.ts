import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IQuarry } from 'app/shared/model/quarry.model';

type EntityResponseType = HttpResponse<IQuarry>;
type EntityArrayResponseType = HttpResponse<IQuarry[]>;

@Injectable({ providedIn: 'root' })
export class QuarryService {
    private resourceUrl = SERVER_API_URL + 'api/quarries';

    constructor(private http: HttpClient) {}

    create(quarry: IQuarry): Observable<EntityResponseType> {
        return this.http.post<IQuarry>(this.resourceUrl, quarry, { observe: 'response' });
    }

    update(quarry: IQuarry): Observable<EntityResponseType> {
        return this.http.put<IQuarry>(this.resourceUrl, quarry, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IQuarry>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IQuarry[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
