import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChecktruckwebSharedModule } from 'app/shared';
import {
    QuarryComponent,
    QuarryDetailComponent,
    QuarryUpdateComponent,
    QuarryDeletePopupComponent,
    QuarryDeleteDialogComponent,
    quarryRoute,
    quarryPopupRoute
} from './';

const ENTITY_STATES = [...quarryRoute, ...quarryPopupRoute];

@NgModule({
    imports: [ChecktruckwebSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [QuarryComponent, QuarryDetailComponent, QuarryUpdateComponent, QuarryDeleteDialogComponent, QuarryDeletePopupComponent],
    entryComponents: [QuarryComponent, QuarryUpdateComponent, QuarryDeleteDialogComponent, QuarryDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebQuarryModule {}
