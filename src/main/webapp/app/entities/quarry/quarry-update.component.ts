import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IQuarry } from 'app/shared/model/quarry.model';
import { QuarryService } from './quarry.service';

@Component({
    selector: 'jhi-quarry-update',
    templateUrl: './quarry-update.component.html'
})
export class QuarryUpdateComponent implements OnInit {
    private _quarry: IQuarry;
    isSaving: boolean;

    constructor(private quarryService: QuarryService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ quarry }) => {
            this.quarry = quarry;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.quarry.id !== undefined) {
            this.subscribeToSaveResponse(this.quarryService.update(this.quarry));
        } else {
            this.subscribeToSaveResponse(this.quarryService.create(this.quarry));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IQuarry>>) {
        result.subscribe((res: HttpResponse<IQuarry>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get quarry() {
        return this._quarry;
    }

    set quarry(quarry: IQuarry) {
        this._quarry = quarry;
    }
}
