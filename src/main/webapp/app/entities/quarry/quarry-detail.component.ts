import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuarry } from 'app/shared/model/quarry.model';

@Component({
    selector: 'jhi-quarry-detail',
    templateUrl: './quarry-detail.component.html'
})
export class QuarryDetailComponent implements OnInit {
    quarry: IQuarry;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ quarry }) => {
            this.quarry = quarry;
        });
    }

    previousState() {
        window.history.back();
    }
}
