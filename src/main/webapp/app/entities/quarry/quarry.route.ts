import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Quarry } from 'app/shared/model/quarry.model';
import { QuarryService } from './quarry.service';
import { QuarryComponent } from './quarry.component';
import { QuarryDetailComponent } from './quarry-detail.component';
import { QuarryUpdateComponent } from './quarry-update.component';
import { QuarryDeletePopupComponent } from './quarry-delete-dialog.component';
import { IQuarry } from 'app/shared/model/quarry.model';

@Injectable({ providedIn: 'root' })
export class QuarryResolve implements Resolve<IQuarry> {
    constructor(private service: QuarryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((quarry: HttpResponse<Quarry>) => quarry.body));
        }
        return of(new Quarry());
    }
}

export const quarryRoute: Routes = [
    {
        path: 'quarry',
        component: QuarryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.quarry.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quarry/:id/view',
        component: QuarryDetailComponent,
        resolve: {
            quarry: QuarryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.quarry.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quarry/new',
        component: QuarryUpdateComponent,
        resolve: {
            quarry: QuarryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.quarry.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quarry/:id/edit',
        component: QuarryUpdateComponent,
        resolve: {
            quarry: QuarryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.quarry.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const quarryPopupRoute: Routes = [
    {
        path: 'quarry/:id/delete',
        component: QuarryDeletePopupComponent,
        resolve: {
            quarry: QuarryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.quarry.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
