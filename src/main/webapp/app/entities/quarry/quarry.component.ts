import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IQuarry } from 'app/shared/model/quarry.model';
import { Principal } from 'app/core';
import { QuarryService } from './quarry.service';

@Component({
    selector: 'jhi-quarry',
    templateUrl: './quarry.component.html'
})
export class QuarryComponent implements OnInit, OnDestroy {
    quarries: IQuarry[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private quarryService: QuarryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.quarryService.query().subscribe(
            (res: HttpResponse<IQuarry[]>) => {
                this.quarries = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInQuarries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IQuarry) {
        return item.id;
    }

    registerChangeInQuarries() {
        this.eventSubscriber = this.eventManager.subscribe('quarryListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
