import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IMaterial } from 'app/shared/model/material.model';
import { Principal } from 'app/core';
import { MaterialService } from './material.service';

@Component({
    selector: 'jhi-material',
    templateUrl: './material.component.html'
})
export class MaterialComponent implements OnInit, OnDestroy {
    materials: IMaterial[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private materialService: MaterialService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.materialService.query().subscribe(
            (res: HttpResponse<IMaterial[]>) => {
                this.materials = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInMaterials();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IMaterial) {
        return item.id;
    }

    registerChangeInMaterials() {
        this.eventSubscriber = this.eventManager.subscribe('materialListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
