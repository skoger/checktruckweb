import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';
import { MaterialQuarryCustomerService } from './material-quarry-customer.service';
import { MaterialQuarryCustomerComponent } from './material-quarry-customer.component';
import { MaterialQuarryCustomerDetailComponent } from './material-quarry-customer-detail.component';
import { MaterialQuarryCustomerUpdateComponent } from './material-quarry-customer-update.component';
import { MaterialQuarryCustomerDeletePopupComponent } from './material-quarry-customer-delete-dialog.component';
import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

@Injectable({ providedIn: 'root' })
export class MaterialQuarryCustomerResolve implements Resolve<IMaterialQuarryCustomer> {
    constructor(private service: MaterialQuarryCustomerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service
                .find(id)
                .pipe(map((materialQuarryCustomer: HttpResponse<MaterialQuarryCustomer>) => materialQuarryCustomer.body));
        }
        return of(new MaterialQuarryCustomer());
    }
}

export const materialQuarryCustomerRoute: Routes = [
    {
        path: 'material-quarry-customer',
        component: MaterialQuarryCustomerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.materialQuarryCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'material-quarry-customer/:id/view',
        component: MaterialQuarryCustomerDetailComponent,
        resolve: {
            materialQuarryCustomer: MaterialQuarryCustomerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.materialQuarryCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'material-quarry-customer/new',
        component: MaterialQuarryCustomerUpdateComponent,
        resolve: {
            materialQuarryCustomer: MaterialQuarryCustomerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.materialQuarryCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'material-quarry-customer/:id/edit',
        component: MaterialQuarryCustomerUpdateComponent,
        resolve: {
            materialQuarryCustomer: MaterialQuarryCustomerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.materialQuarryCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const materialQuarryCustomerPopupRoute: Routes = [
    {
        path: 'material-quarry-customer/:id/delete',
        component: MaterialQuarryCustomerDeletePopupComponent,
        resolve: {
            materialQuarryCustomer: MaterialQuarryCustomerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'checktruckwebApp.materialQuarryCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
