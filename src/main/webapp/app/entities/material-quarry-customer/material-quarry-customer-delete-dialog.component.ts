import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';
import { MaterialQuarryCustomerService } from './material-quarry-customer.service';

@Component({
    selector: 'jhi-material-quarry-customer-delete-dialog',
    templateUrl: './material-quarry-customer-delete-dialog.component.html'
})
export class MaterialQuarryCustomerDeleteDialogComponent {
    materialQuarryCustomer: IMaterialQuarryCustomer;

    constructor(
        private materialQuarryCustomerService: MaterialQuarryCustomerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.materialQuarryCustomerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'materialQuarryCustomerListModification',
                content: 'Deleted an materialQuarryCustomer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-material-quarry-customer-delete-popup',
    template: ''
})
export class MaterialQuarryCustomerDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ materialQuarryCustomer }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(MaterialQuarryCustomerDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.materialQuarryCustomer = materialQuarryCustomer;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
