import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

@Component({
    selector: 'jhi-material-quarry-customer-detail',
    templateUrl: './material-quarry-customer-detail.component.html'
})
export class MaterialQuarryCustomerDetailComponent implements OnInit {
    materialQuarryCustomer: IMaterialQuarryCustomer;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ materialQuarryCustomer }) => {
            this.materialQuarryCustomer = materialQuarryCustomer;
        });
    }

    previousState() {
        window.history.back();
    }
}
