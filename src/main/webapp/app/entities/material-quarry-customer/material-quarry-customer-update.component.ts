import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';
import { MaterialQuarryCustomerService } from './material-quarry-customer.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer';
import { IQuarry } from 'app/shared/model/quarry.model';
import { QuarryService } from 'app/entities/quarry';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
    selector: 'jhi-material-quarry-customer-update',
    templateUrl: './material-quarry-customer-update.component.html'
})
export class MaterialQuarryCustomerUpdateComponent implements OnInit {
    private _materialQuarryCustomer: IMaterialQuarryCustomer;
    isSaving: boolean;

    customers: ICustomer[];

    quarries: IQuarry[];

    materials: IMaterial[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private materialQuarryCustomerService: MaterialQuarryCustomerService,
        private customerService: CustomerService,
        private quarryService: QuarryService,
        private materialService: MaterialService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ materialQuarryCustomer }) => {
            this.materialQuarryCustomer = materialQuarryCustomer;
        });
        this.customerService.query().subscribe(
            (res: HttpResponse<ICustomer[]>) => {
                this.customers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.quarryService.query().subscribe(
            (res: HttpResponse<IQuarry[]>) => {
                this.quarries = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.materialService.query().subscribe(
            (res: HttpResponse<IMaterial[]>) => {
                this.materials = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.materialQuarryCustomer.id !== undefined) {
            this.subscribeToSaveResponse(this.materialQuarryCustomerService.update(this.materialQuarryCustomer));
        } else {
            this.subscribeToSaveResponse(this.materialQuarryCustomerService.create(this.materialQuarryCustomer));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialQuarryCustomer>>) {
        result.subscribe(
            (res: HttpResponse<IMaterialQuarryCustomer>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCustomerById(index: number, item: ICustomer) {
        return item.id;
    }

    trackQuarryById(index: number, item: IQuarry) {
        return item.id;
    }

    trackMaterialById(index: number, item: IMaterial) {
        return item.id;
    }
    get materialQuarryCustomer() {
        return this._materialQuarryCustomer;
    }

    set materialQuarryCustomer(materialQuarryCustomer: IMaterialQuarryCustomer) {
        this._materialQuarryCustomer = materialQuarryCustomer;
    }
}
