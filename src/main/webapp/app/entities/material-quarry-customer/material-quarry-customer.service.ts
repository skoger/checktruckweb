import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';

type EntityResponseType = HttpResponse<IMaterialQuarryCustomer>;
type EntityArrayResponseType = HttpResponse<IMaterialQuarryCustomer[]>;

@Injectable({ providedIn: 'root' })
export class MaterialQuarryCustomerService {
    private resourceUrl = SERVER_API_URL + 'api/material-quarry-customers';

    constructor(private http: HttpClient) {}

    create(materialQuarryCustomer: IMaterialQuarryCustomer): Observable<EntityResponseType> {
        return this.http.post<IMaterialQuarryCustomer>(this.resourceUrl, materialQuarryCustomer, { observe: 'response' });
    }

    update(materialQuarryCustomer: IMaterialQuarryCustomer): Observable<EntityResponseType> {
        return this.http.put<IMaterialQuarryCustomer>(this.resourceUrl, materialQuarryCustomer, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IMaterialQuarryCustomer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IMaterialQuarryCustomer[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
