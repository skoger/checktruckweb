import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IMaterialQuarryCustomer } from 'app/shared/model/material-quarry-customer.model';
import { Principal } from 'app/core';
import { MaterialQuarryCustomerService } from './material-quarry-customer.service';

@Component({
    selector: 'jhi-material-quarry-customer',
    templateUrl: './material-quarry-customer.component.html'
})
export class MaterialQuarryCustomerComponent implements OnInit, OnDestroy {
    materialQuarryCustomers: IMaterialQuarryCustomer[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private materialQuarryCustomerService: MaterialQuarryCustomerService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.materialQuarryCustomerService.query().subscribe(
            (res: HttpResponse<IMaterialQuarryCustomer[]>) => {
                this.materialQuarryCustomers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInMaterialQuarryCustomers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IMaterialQuarryCustomer) {
        return item.id;
    }

    registerChangeInMaterialQuarryCustomers() {
        this.eventSubscriber = this.eventManager.subscribe('materialQuarryCustomerListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
