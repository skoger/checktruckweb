import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChecktruckwebSharedModule } from 'app/shared';
import {
    MaterialQuarryCustomerComponent,
    MaterialQuarryCustomerDetailComponent,
    MaterialQuarryCustomerUpdateComponent,
    MaterialQuarryCustomerDeletePopupComponent,
    MaterialQuarryCustomerDeleteDialogComponent,
    materialQuarryCustomerRoute,
    materialQuarryCustomerPopupRoute
} from './';

const ENTITY_STATES = [...materialQuarryCustomerRoute, ...materialQuarryCustomerPopupRoute];

@NgModule({
    imports: [ChecktruckwebSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        MaterialQuarryCustomerComponent,
        MaterialQuarryCustomerDetailComponent,
        MaterialQuarryCustomerUpdateComponent,
        MaterialQuarryCustomerDeleteDialogComponent,
        MaterialQuarryCustomerDeletePopupComponent
    ],
    entryComponents: [
        MaterialQuarryCustomerComponent,
        MaterialQuarryCustomerUpdateComponent,
        MaterialQuarryCustomerDeleteDialogComponent,
        MaterialQuarryCustomerDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChecktruckwebMaterialQuarryCustomerModule {}
