import { IVehicle } from 'app/shared/model//vehicle.model';

export interface IBrand {
    id?: number;
    description?: string;
    idbrands?: IVehicle[];
}

export class Brand implements IBrand {
    constructor(public id?: number, public description?: string, public idbrands?: IVehicle[]) {}
}
