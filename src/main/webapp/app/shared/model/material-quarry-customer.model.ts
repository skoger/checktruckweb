import { ICustomer } from 'app/shared/model//customer.model';
import { IQuarry } from 'app/shared/model//quarry.model';
import { IMaterial } from 'app/shared/model//material.model';
import { IRoadmap } from 'app/shared/model//roadmap.model';

export interface IMaterialQuarryCustomer {
    id?: number;
    price?: number;
    customer?: ICustomer;
    quarry?: IQuarry;
    material?: IMaterial;
    idmaterialquarries?: IRoadmap[];
}

export class MaterialQuarryCustomer implements IMaterialQuarryCustomer {
    constructor(
        public id?: number,
        public price?: number,
        public customer?: ICustomer,
        public quarry?: IQuarry,
        public material?: IMaterial,
        public idmaterialquarries?: IRoadmap[]
    ) {}
}
