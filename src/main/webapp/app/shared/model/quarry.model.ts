import { IMaterialQuarryCustomer } from 'app/shared/model//material-quarry-customer.model';
import { IVehicle } from 'app/shared/model//vehicle.model';

export interface IQuarry {
    id?: number;
    description?: string;
    idquarries?: IMaterialQuarryCustomer[];
    vehicles?: IVehicle[];
}

export class Quarry implements IQuarry {
    constructor(
        public id?: number,
        public description?: string,
        public idquarries?: IMaterialQuarryCustomer[],
        public vehicles?: IVehicle[]
    ) {}
}
