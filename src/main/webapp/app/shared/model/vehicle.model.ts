import { ICarrier } from 'app/shared/model//carrier.model';
import { ICapacity } from 'app/shared/model//capacity.model';
import { IBrand } from 'app/shared/model//brand.model';
import { IRoadmap } from 'app/shared/model//roadmap.model';
import { IQuarry } from 'app/shared/model//quarry.model';

export interface IVehicle {
    id?: number;
    licensePlate?: string;
    quantity?: number;
    model?: string;
    carrier?: ICarrier;
    capacity?: ICapacity;
    brand?: IBrand;
    idvehicles?: IRoadmap[];
    quarry?: IQuarry;
}

export class Vehicle implements IVehicle {
    constructor(
        public id?: number,
        public licensePlate?: string,
        public quantity?: number,
        public model?: string,
        public carrier?: ICarrier,
        public capacity?: ICapacity,
        public brand?: IBrand,
        public idvehicles?: IRoadmap[],
        public quarry?: IQuarry
    ) {}
}
