import { IVehicle } from 'app/shared/model//vehicle.model';

export interface ICapacity {
    id?: number;
    description?: string;
    idcapacities?: IVehicle[];
}

export class Capacity implements ICapacity {
    constructor(public id?: number, public description?: string, public idcapacities?: IVehicle[]) {}
}
