import { Moment } from 'moment';
import { IRouters } from 'app/shared/model//routers.model';
import { IVehicle } from 'app/shared/model//vehicle.model';
import { IMaterialQuarryCustomer } from 'app/shared/model//material-quarry-customer.model';

export interface IRoadmap {
    id?: number;
    price?: number;
    synch?: boolean;
    verified?: boolean;
    idUser?: string;
    created?: Moment;
    verifiedDate?: Moment;
    userKey?: string;
    userName?: string;
    annulled?: boolean;
    routers?: IRouters;
    vehicle?: IVehicle;
    materialQuarryCustomer?: IMaterialQuarryCustomer;
}

export class Roadmap implements IRoadmap {
    constructor(
        public id?: number,
        public price?: number,
        public synch?: boolean,
        public verified?: boolean,
        public idUser?: string,
        public created?: Moment,
        public verifiedDate?: Moment,
        public userKey?: string,
        public userName?: string,
        public annulled?: boolean,
        public routers?: IRouters,
        public vehicle?: IVehicle,
        public materialQuarryCustomer?: IMaterialQuarryCustomer
    ) {
        this.synch = false;
        this.verified = false;
        this.annulled = false;
    }
}
