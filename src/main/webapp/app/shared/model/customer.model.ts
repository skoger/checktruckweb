import { IMaterialQuarryCustomer } from 'app/shared/model//material-quarry-customer.model';

export interface ICustomer {
    id?: number;
    businessName?: string;
    nit?: string;
    email?: string;
    address?: string;
    phone?: string;
    contact?: string;
    contactPhone?: string;
    idcustomers?: IMaterialQuarryCustomer[];
}

export class Customer implements ICustomer {
    constructor(
        public id?: number,
        public businessName?: string,
        public nit?: string,
        public email?: string,
        public address?: string,
        public phone?: string,
        public contact?: string,
        public contactPhone?: string,
        public idcustomers?: IMaterialQuarryCustomer[]
    ) {}
}
