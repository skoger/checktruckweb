import { IMaterialQuarryCustomer } from 'app/shared/model//material-quarry-customer.model';

export interface IMaterial {
    id?: number;
    description?: string;
    idmaterials?: IMaterialQuarryCustomer[];
}

export class Material implements IMaterial {
    constructor(public id?: number, public description?: string, public idmaterials?: IMaterialQuarryCustomer[]) {}
}
