import { IVehicle } from 'app/shared/model//vehicle.model';

export interface ICarrier {
    id?: number;
    businessName?: string;
    address?: string;
    phone?: string;
    contact?: string;
    contactPhone?: string;
    idcarriers?: IVehicle[];
}

export class Carrier implements ICarrier {
    constructor(
        public id?: number,
        public businessName?: string,
        public address?: string,
        public phone?: string,
        public contact?: string,
        public contactPhone?: string,
        public idcarriers?: IVehicle[]
    ) {}
}
