import { IRoadmap } from 'app/shared/model//roadmap.model';

export interface IRouters {
    id?: number;
    description?: string;
    distanceKm?: number;
    cost?: number;
    reportCopies?: number;
    routersRaodmaps?: IRoadmap[];
}

export class Routers implements IRouters {
    constructor(
        public id?: number,
        public description?: string,
        public distanceKm?: number,
        public cost?: number,
        public reportCopies?: number,
        public routersRaodmaps?: IRoadmap[]
    ) {}
}
